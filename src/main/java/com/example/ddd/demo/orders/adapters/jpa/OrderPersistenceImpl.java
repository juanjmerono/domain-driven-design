package com.example.ddd.demo.orders.adapters.jpa;

import java.util.Optional;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.orders.adapters.jpa.entity.OrderEntity;
import com.example.ddd.demo.orders.adapters.jpa.entity.OrderUserEntity;
import com.example.ddd.demo.orders.application.OrderPersistence;
import com.example.ddd.demo.orders.domain.event.OrderEvent;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;

@Service
public class OrderPersistenceImpl implements OrderPersistence {

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private OrderUserRepository orderUserRepository;

    @Autowired
    private OrderBookDataResolver bookDataResolver;

    @Override
    public Iterable<Order> findAllByUserId(String userId, int page, int page_size) {
        return orderRepository.findAllByOrderUserId(userId,PageRequest.of(page,page_size)).map(i -> i.toModel(bookDataResolver));
    }

    @Override
    public Iterable<Order> findAll(int page, int page_size) {
        return orderRepository.findAll(PageRequest.of(page,page_size)).map(i -> i.toModel(bookDataResolver));
    }


    @Override
    public Optional<Order> findByIdAndUserId(Long id, String userId) {
        return orderRepository.findByIdAndOrderUserId(id, userId).map(i -> i.toModel(bookDataResolver));
    }

    private Order save(Order order) {
        OrderUserEntity oue = orderUserRepository.findById(order.getUser().getUserId()).orElse(OrderUserEntity.of(order.getUser()));
        return orderRepository.save(OrderEntity.of(order,oue)).toModel(bookDataResolver);
    }

    @Transactional
    @Override
    public Order createOrder(Order order) {
        return save(order);
    }

    @Transactional
    @Override
    public Order updateOrder(Order order) {
        return save(order);
    }

    @Transactional
    @Override
    public void deleteOrder(Order order) {
        orderRepository.findById(order.getId()).ifPresent(o -> orderRepository.delete(o));
    }

}
