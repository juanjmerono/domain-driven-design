package com.example.ddd.demo.orders.application;

import com.example.ddd.demo.orders.domain.event.OrderEvent;

public interface OrderEventPublisher {
    
    public void publishEvent(OrderEvent oe);

}
