package com.example.ddd.demo.orders.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.orders.application.command.OrderCommandExecutor;
import com.example.ddd.demo.orders.domain.exception.OrderItemNotValidException;
import com.example.ddd.demo.orders.domain.exception.OrderNotFoundException;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;

@Service
public class CommandOrdersUseCase {
    @Autowired
    private OrderPersistence orderPersistence;

    @Autowired
    private RemoteUserPersistence remoteUserPersistence;

    @Autowired
    private OrderBookDataResolver bookDataResolver;

    @Autowired
    private OrderEventPublisher orderEventPublisher;

    public Optional<Order> executeCommand(OrderCommandExecutor orderCommandExecutor) throws OrderNotFoundException, OrderItemNotValidException, OrderUserNotFoundException {
        return orderCommandExecutor.executeCommand(orderPersistence, remoteUserPersistence, bookDataResolver, orderEventPublisher);
    }

}
