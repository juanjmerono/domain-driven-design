package com.example.ddd.demo.orders.domain.model;

import java.util.Objects;

public class OrderUser {
    
    private String userId;
    private String userNick;
    private String userEmail;

    private OrderUser(String uid, String unick, String uemail) {
        this.userId = uid;
        this.userNick = unick;
        this.userEmail = uemail;
    }

    public static OrderUser of(String uid, String un, String ue) {
        Objects.requireNonNull(uid);
        Objects.requireNonNull(ue);
        if (ue.chars().filter(c->c=='@').count()!=1) throw new IllegalArgumentException();
        return new OrderUser(uid,un,ue);
    }

    public String getUserId() { return this.userId; }
    public String getUserNick() { return this.userNick; }
    public String getUserEmail() { return this.userEmail; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderUser) {
            OrderUser other = (OrderUser)obj;
            return Objects.equals(this.userId, other.userId);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.userId);
    }

    @Override
    public String toString() {
        return String.format("%s %s %s", userId,userNick,userEmail);
    }
}
