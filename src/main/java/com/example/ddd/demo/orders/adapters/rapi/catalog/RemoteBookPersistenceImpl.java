package com.example.ddd.demo.orders.adapters.rapi.catalog;

import java.math.BigDecimal;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.CacheManager;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.example.ddd.demo.orders.application.RemoteBookPersistence;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;

@Service
public class RemoteBookPersistenceImpl implements RemoteBookPersistence {

    @Autowired
    private RestTemplate restTemplate;

    @Autowired
    private CacheManager cacheManager;
    
    @Value("${catalog.api.url}")
    private String apiCatalog;

    private Optional<RemoteBookAPI> findBookById(String bookId) {
        try {
            return Optional.of(restTemplate
                .getForEntity(apiCatalog+"/"+bookId, RemoteBookAPI.class)
                .getBody());
        } catch (NotFound nfe) {
            return Optional.empty();
        }
    }

    @Override
    @Cacheable(cacheNames = "bookcache", key = "#bookId")
    public RemoteBookAPI loadBook(String bookId) throws OrderBookNotFound {
        return findBookById(bookId).orElseThrow(() -> new OrderBookNotFound());
    }

    // Delete caches every 6 hours by default
    @Scheduled(fixedDelayString = "${cache.clear.interval:21600000}",initialDelayString = "${cache.clear.initialDelay:21600000}")
    public void evictAllCaches() {
        cacheManager.getCacheNames().stream().forEach(cacheName -> cacheManager.getCache(cacheName).clear());
    }
    
}
