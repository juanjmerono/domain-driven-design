package com.example.ddd.demo.orders.adapters.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.orders.application.OrderEventPublisher;
import com.example.ddd.demo.orders.domain.event.OrderEvent;

@Service
public class OrderEventPublisherImpl implements OrderEventPublisher {

    @Autowired
    private ApplicationEventPublisher eventPublisher;


    @Override
    public void publishEvent(OrderEvent oe) {
        eventPublisher.publishEvent(oe);
    }
    
}
