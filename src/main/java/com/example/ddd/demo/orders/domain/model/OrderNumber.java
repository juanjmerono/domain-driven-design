package com.example.ddd.demo.orders.domain.model;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.Formattable;
import java.util.Formatter;
import java.util.Objects;

public class OrderNumber implements Serializable, Formattable {

    private int number;

    // For Persistence
    OrderNumber() {}

    private OrderNumber(int n) {
        this.number = n;
    }

    public static OrderNumber of() {
        // Create order number by day
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyD");  
        return new OrderNumber(Integer.parseInt(LocalDateTime.now().format(dtf)));
    }

    public static OrderNumber of(int n) {
        // Create if number > 0 from given number
        return (n<=0)?OrderNumber.of():new OrderNumber(n);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderNumber) {
            OrderNumber other = (OrderNumber)obj;
            return Objects.equals(this.number, other.number);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hashCode(this.number);
    }

    @Override
    public String toString() {
        return String.format("OrderNumber %d",this.number);
    }
    @Override
    public void formatTo(Formatter formatter, int flags, int width, int precision) {
        formatter.format("%d", this.number);
    }

    public Integer toInteger() { return this.number; }

}
