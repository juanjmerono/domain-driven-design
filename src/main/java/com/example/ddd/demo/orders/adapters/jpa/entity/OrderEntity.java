package com.example.ddd.demo.orders.adapters.jpa.entity;

import java.io.Serializable;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.CascadeType;
import javax.persistence.CollectionTable;
import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Embedded;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="ORDERS")
public class OrderEntity implements Serializable {
    
    @Id
    @GeneratedValue
    private long id;

    @Embedded
    private OrderNumberValueObject orderNumber;

    @ManyToOne(optional = false, cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @JoinColumn(updatable = true)
    private OrderUserEntity orderUser;

    @ElementCollection
    @CollectionTable(name="ORDER_ITEMS",joinColumns = @JoinColumn(name="order_id"))
    @Column(name="item")
    private Set<OrderItemValueObject> items;

    public static OrderEntity of(Order original, OrderUserEntity oue) {
        return OrderEntity.builder()
            .id(Objects.isNull(original.getId())?0:original.getId())
            .orderNumber(OrderNumberValueObject.of(original.getOrderNumber()))
            .items(original.getItems().stream().map(OrderItemValueObject::of).collect(Collectors.toSet()))
            .orderUser(oue)
            .build();
    }

    public static OrderEntity of(Order original) {
        return OrderEntity.of(original, OrderUserEntity.of(original.getUser()));
    }

    public Order toModel(OrderBookDataResolver resolver) {
        return Order.of(this.id,
            this.orderNumber.toModel(),
            this.items.stream().map(i -> i.toModel(resolver)).collect(Collectors.toSet()),
            this.orderUser.toModel());
    }

}
