package com.example.ddd.demo.orders.adapters.rest.dto;

import java.math.BigDecimal;

import com.example.ddd.demo.orders.domain.model.OrderItem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderItemDTO {
    private String bookId;
    private int amount;
    private BigDecimal price;
    private String title;

    public static OrderItemDTO of(OrderItem o) {
        return OrderItemDTO.builder()
                .bookId(o.getBookId())
                .amount(o.getAmount())
                .price(o.getPrice())
                .title(o.getBookTitle())
                .build();
    }
}
