package com.example.ddd.demo.orders.adapters.rest;

import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;

@Component
public class OrderModelAssembler implements RepresentationModelAssembler<OrderDTO,EntityModel<OrderDTO>>{

    @Autowired
    private PagedResourcesAssembler<OrderDTO> pagedResourcesAssembler;

    @Override
    public EntityModel<OrderDTO> toModel(OrderDTO entity) {
        return EntityModel.of(entity,
            linkTo(OrderRestController.class).slash(entity.getId()).withSelfRel(),
            linkTo(OrderRestController.class).slash(entity.getId()).withRel("put"),
            linkTo(OrderRestController.class).slash(entity.getId()).withRel("delete")
        );
    }

    @Override
    public CollectionModel<EntityModel<OrderDTO>> toCollectionModel(Iterable<? extends OrderDTO> entities) {
        PagedModel<EntityModel<OrderDTO>> model = pagedResourcesAssembler.toModel((Page)entities, this);
        model.add(linkTo(methodOn(OrderRestController.class).createOrder(null,null)).withRel("post"));
        return model;    
    }

}
