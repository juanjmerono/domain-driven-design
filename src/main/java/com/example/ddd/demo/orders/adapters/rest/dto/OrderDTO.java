package com.example.ddd.demo.orders.adapters.rest.dto;

import java.math.BigDecimal;
import java.util.Set;
import java.util.stream.Collectors;

import com.example.ddd.demo.orders.domain.model.Order;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderDTO {
    private long id;
    private int orderNumber;
    private Set<OrderItemDTO> items;
    private int totalAmount;
    private BigDecimal totalPrice;
    private String userEmail;
    private String userNick;

    public static OrderDTO of (Order original) {
        return OrderDTO.builder()
            .id(original.getId())
            .orderNumber(original.getOrderNumber())
            .items(original.getItems().stream().map(OrderItemDTO::of).collect(Collectors.toSet()))
            .totalAmount(original.getTotalAmount())
            .totalPrice(original.getTotalPrice())
            .userEmail(original.getUser().getUserEmail())
            .userNick(original.getUser().getUserNick())
            .build();
    }
}
