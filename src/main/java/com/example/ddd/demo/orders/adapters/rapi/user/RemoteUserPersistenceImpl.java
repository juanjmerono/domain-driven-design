package com.example.ddd.demo.orders.adapters.rapi.user;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.example.ddd.demo.orders.application.RemoteUserPersistence;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.OrderUser;

@Service
public class RemoteUserPersistenceImpl implements RemoteUserPersistence {

    @Autowired
    private RestTemplate restTemplate;
    
    @Value("${users.api.url}")
    private String apiUsers;

    public Optional<RemoteUserAPI> findOrderUserData(String userId) {
        try {
            return Optional.of(restTemplate
                .getForEntity(apiUsers+"/"+userId, RemoteUserAPI.class)
                .getBody());
        } catch (NotFound nfe) {
            return Optional.empty();
        }
    }

    @Override
    @Cacheable(cacheNames = "usercache", key = "#userId")
    public Optional<OrderUser> loadUser(String userId) throws OrderUserNotFoundException {
        return Optional.of(this.findOrderUserData(userId).orElseThrow(() -> new OrderUserNotFoundException()).toModel());
    }

    
}
