package com.example.ddd.demo.orders.application.command;

import java.util.Arrays;
import java.util.List;
import java.util.Optional;

import com.example.ddd.demo.orders.application.OrderEventPublisher;
import com.example.ddd.demo.orders.application.OrderPersistence;
import com.example.ddd.demo.orders.application.RemoteUserPersistence;
import com.example.ddd.demo.orders.domain.event.OrderEvent;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;
import com.example.ddd.demo.orders.domain.model.OrderUser;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class CreateOrderCommand implements OrderCommandExecutor {

    @JsonIgnore
    private final String userId;
    private final String bookId;
    private final Integer amount;


    public static CreateOrderCommand of (String bookId, Integer amount) {
        return new CreateOrderCommand(null, bookId, amount);
    }

    public static CreateOrderCommand of (String uid, CreateOrderCommand original) {
        return new CreateOrderCommand(uid, original.getBookId(), original.getAmount());
    }

    @Override
    public Optional<Order> executeCommand(OrderPersistence orderPersistence,
            RemoteUserPersistence remoteUserPersistence, OrderBookDataResolver orderBookDataResolver,
            OrderEventPublisher orderEventPublisher) throws OrderUserNotFoundException {
        List<OrderItem> oi = Arrays.asList(new OrderItem[]{OrderItem.of(this.bookId,this.amount,orderBookDataResolver)});
        OrderUser ou = remoteUserPersistence.loadUser(this.userId).orElseThrow(() -> new OrderUserNotFoundException());
        Order newOrder = Order.of(ou);
        this.addOrderItems(newOrder, oi);
        Order savedOrder = orderPersistence.createOrder(newOrder);
        orderEventPublisher.publishEvent(OrderEvent.of(savedOrder,OrderEvent.EventType.NEW_ORDER));
        return Optional.of(savedOrder);
    }
    
}
