package com.example.ddd.demo.orders.application.command;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import com.example.ddd.demo.orders.application.OrderEventPublisher;
import com.example.ddd.demo.orders.application.OrderPersistence;
import com.example.ddd.demo.orders.application.RemoteUserPersistence;
import com.example.ddd.demo.orders.domain.event.OrderEvent;
import com.example.ddd.demo.orders.domain.exception.OrderNotFoundException;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class UpdateOrderCommand implements OrderCommandExecutor {

    @JsonIgnore
    private final Long orderId;
    @JsonIgnore
    private final String userId;
    private final UpdateOrderItem [] updateOrderItems;

    public static UpdateOrderCommand of (String bookId, Integer amount) {
        return new UpdateOrderCommand(null, null, new UpdateOrderItem[]{UpdateOrderItem.of(bookId, amount)});
    }

    public static UpdateOrderCommand of (UpdateOrderItem [] updateOrderItems) {
        return new UpdateOrderCommand(null, null, updateOrderItems);
    }

    public static UpdateOrderCommand of (Long id, String uid, UpdateOrderCommand original) {
        return new UpdateOrderCommand(id, uid, original.getUpdateOrderItems());
    }

    @Override
    public Optional<Order> executeCommand(OrderPersistence orderPersistence,
            RemoteUserPersistence remoteUserPersistence, OrderBookDataResolver orderBookDataResolver,
            OrderEventPublisher orderEventPublisher) throws OrderUserNotFoundException, OrderNotFoundException {
        List<OrderItem> orderItems = new ArrayList<OrderItem>();
        for (UpdateOrderItem uoi: updateOrderItems) {
            orderItems.add(OrderItem.of(uoi.getBookId(),uoi.getAmount(),orderBookDataResolver));
        }
        Order currentOrder = orderPersistence.findByIdAndUserId(this.orderId,this.userId).orElseThrow(() -> new OrderNotFoundException());
        this.addOrderItems(currentOrder, orderItems);
        Order savedOrder = orderPersistence.updateOrder(currentOrder);
        orderEventPublisher.publishEvent(OrderEvent.of(savedOrder,OrderEvent.EventType.UPDATE_ORDER));
        return Optional.of(savedOrder);

    }
    
}
