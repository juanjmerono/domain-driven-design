package com.example.ddd.demo.orders.adapters.jpa;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.example.ddd.demo.orders.adapters.jpa.entity.OrderUserEntity;

@Repository
public interface OrderUserRepository extends JpaRepository<OrderUserEntity,String>{
    
}
