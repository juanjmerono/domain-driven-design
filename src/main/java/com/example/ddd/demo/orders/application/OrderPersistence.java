package com.example.ddd.demo.orders.application;

import java.util.Optional;

import com.example.ddd.demo.orders.domain.model.Order;

public interface OrderPersistence {

    Iterable<Order> findAllByUserId(String userId, int page, int page_size);

    Iterable<Order> findAll(int page, int page_size);

    Optional<Order> findByIdAndUserId(Long id, String userId);

    Order createOrder(Order order);

    Order updateOrder(Order order);

    void deleteOrder(Order order);

}
