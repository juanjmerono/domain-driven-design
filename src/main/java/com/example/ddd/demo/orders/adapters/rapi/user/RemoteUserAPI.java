package com.example.ddd.demo.orders.adapters.rapi.user;

import com.example.ddd.demo.orders.domain.model.OrderUser;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteUserAPI {
    private String id;
    private String nick;
    private String email;

    public OrderUser toModel() {
        return OrderUser.of(this.id, this.nick, this.email);
    }
}
