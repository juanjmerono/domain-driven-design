package com.example.ddd.demo.orders.adapters.jpa;

import java.util.Optional;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.ddd.demo.orders.adapters.jpa.entity.OrderEntity;

@Repository
public interface OrderRepository extends PagingAndSortingRepository<OrderEntity,Long> {

    public Page<OrderEntity> findAllByOrderUserId(String id, Pageable pageable);

    public Optional<OrderEntity> findByIdAndOrderUserId(Long id, String userId);
    
}
