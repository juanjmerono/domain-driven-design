package com.example.ddd.demo.orders.adapters.listener;

import org.springframework.stereotype.Service;
import org.springframework.transaction.event.TransactionPhase;
import org.springframework.transaction.event.TransactionalEventListener;

import com.example.ddd.demo.orders.domain.event.OrderEvent;

import lombok.extern.log4j.Log4j2;

@Log4j2
@Service
public class OrderEventListener {
    
    @TransactionalEventListener(phase = TransactionPhase.AFTER_COMPLETION)
    public void updateOrderUser(OrderEvent u) {
        log.info("EVENT: "+u.toString());
    }

}
