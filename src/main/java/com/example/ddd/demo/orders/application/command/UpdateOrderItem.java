package com.example.ddd.demo.orders.application.command;

import lombok.Data;

@Data
public class UpdateOrderItem {
    private final String bookId;
    private final Integer amount;

    public static UpdateOrderItem of(String bookId, Integer amount) {
        return new UpdateOrderItem(bookId, amount);
    }
}
