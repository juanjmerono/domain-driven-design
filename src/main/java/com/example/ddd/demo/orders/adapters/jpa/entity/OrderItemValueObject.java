package com.example.ddd.demo.orders.adapters.jpa.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import javax.persistence.Embeddable;

import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OrderItemValueObject implements Serializable {

    private String bookId;

    private Integer amount;

    private BigDecimal price;

    public static OrderItemValueObject of(OrderItem o) {
        return OrderItemValueObject.builder()
            .bookId(o.getBookId())
            .amount(o.getAmount())
            .price(o.getPrice())
            .build();
    }

    public OrderItem toModel(OrderBookDataResolver resolver) {
        return OrderItem.of(this.bookId, this.amount, resolver);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderItem) {
            OrderItemValueObject other = (OrderItemValueObject)obj;
            return Objects.equals(this.bookId, other.bookId) 
                && Objects.equals(this.price,other.price);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId,price);
    }

}
