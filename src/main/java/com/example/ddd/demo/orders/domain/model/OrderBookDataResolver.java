package com.example.ddd.demo.orders.domain.model;

import java.math.BigDecimal;

import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;

public interface OrderBookDataResolver {
    public String getBookTitle(String bookId) throws OrderBookNotFound;
    public BigDecimal getBookPrice(String bookId) throws OrderBookNotFound;
}
