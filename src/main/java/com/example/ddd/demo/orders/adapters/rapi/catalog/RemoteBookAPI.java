package com.example.ddd.demo.orders.adapters.rapi.catalog;

import java.io.Serializable;
import java.math.BigDecimal;

import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true)
public class RemoteBookAPI implements Serializable {

    private String id;
    private String title;
    @JsonFormat(shape=JsonFormat.Shape.STRING)
    private BigDecimal price;
    
}
