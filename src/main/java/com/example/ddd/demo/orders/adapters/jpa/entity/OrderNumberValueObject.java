package com.example.ddd.demo.orders.adapters.jpa.entity;

import java.io.Serializable;

import javax.persistence.Embeddable;

import com.example.ddd.demo.orders.domain.model.OrderNumber;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Embeddable
public class OrderNumberValueObject implements Serializable {

    private int number;

    public static OrderNumberValueObject of(Integer n) {
        return OrderNumberValueObject.builder().number(n).build();
    }

    public OrderNumber toModel() {
        return OrderNumber.of(number);
    }

}
