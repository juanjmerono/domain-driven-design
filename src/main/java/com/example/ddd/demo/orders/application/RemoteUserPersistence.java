package com.example.ddd.demo.orders.application;

import java.util.Optional;

import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.OrderUser;

public interface RemoteUserPersistence {
    
    public Optional<OrderUser> loadUser(String userId) throws OrderUserNotFoundException;

}
