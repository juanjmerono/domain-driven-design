package com.example.ddd.demo.orders.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;
import com.example.ddd.demo.orders.domain.model.Order;

@Service
public class QueryOrdersUseCase {

    public static final String PAGE_SIZE = "3";

    @Autowired
    private OrderPersistence orderPersistence;

    public Optional<OrderDTO> findById(Long id, String userId) {
        return orderPersistence.findByIdAndUserId(id,userId).map(OrderDTO::of);
    }
    
    public Iterable<Order> findAllByUser(String userId, int page, int page_size) {
        return orderPersistence.findAllByUserId(userId, page, page_size);
    }

    public Iterable<Order> findAll(int page, int page_size) {
        return orderPersistence.findAll(page, page_size);
    }
   
}
