package com.example.ddd.demo.orders.adapters.jpa.entity;

import java.io.Serializable;
import java.util.Objects;

import javax.persistence.Entity;
import javax.persistence.Id;

import com.example.ddd.demo.orders.domain.model.OrderUser;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
public class OrderUserEntity implements Serializable {

    @Id
    private String id;
    private String nick;
    private String email;

    public static OrderUserEntity of(OrderUser ou) {
        return OrderUserEntity.builder()
            .id(ou.getUserId())
            .nick(ou.getUserNick())
            .email(ou.getUserEmail())
            .build();
    }

    public OrderUser toModel() {
        return OrderUser.of(this.id,this.nick,this.email);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderUserEntity) {
            OrderUserEntity other = (OrderUserEntity)obj;
            return Objects.equals(this.id, other.id);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(this.id);
    }
    

}
