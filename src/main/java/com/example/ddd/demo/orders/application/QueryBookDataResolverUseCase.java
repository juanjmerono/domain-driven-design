package com.example.ddd.demo.orders.application;

import java.math.BigDecimal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;

@Service
public class QueryBookDataResolverUseCase implements OrderBookDataResolver {

    @Autowired
    private RemoteBookPersistence remoteBookPersistence;

    @Override
    public String getBookTitle(String bookId) throws OrderBookNotFound {
        return remoteBookPersistence.loadBook(bookId).getTitle();
    }

    @Override
    public BigDecimal getBookPrice(String bookId) throws OrderBookNotFound {
        return remoteBookPersistence.loadBook(bookId).getPrice();
    }
    
}
