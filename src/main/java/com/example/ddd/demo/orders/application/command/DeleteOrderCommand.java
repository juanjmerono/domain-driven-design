package com.example.ddd.demo.orders.application.command;

import java.util.Optional;

import com.example.ddd.demo.orders.application.OrderEventPublisher;
import com.example.ddd.demo.orders.application.OrderPersistence;
import com.example.ddd.demo.orders.application.RemoteUserPersistence;
import com.example.ddd.demo.orders.domain.event.OrderEvent;
import com.example.ddd.demo.orders.domain.exception.OrderNotFoundException;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class DeleteOrderCommand implements OrderCommandExecutor {

    @JsonIgnore
    private final Long orderId;
    @JsonIgnore
    private final String userId;

    public static DeleteOrderCommand of (Long id, String uid) {
        return new DeleteOrderCommand(id, uid);
    }

    @Override
    public Optional<Order> executeCommand(OrderPersistence orderPersistence,
            RemoteUserPersistence remoteUserPersistence, OrderBookDataResolver orderBookDataResolver,
            OrderEventPublisher orderEventPublisher) throws OrderUserNotFoundException, OrderNotFoundException {
        Order currentOrder = orderPersistence.findByIdAndUserId(this.orderId,this.userId).orElseThrow(() -> new OrderNotFoundException());
        orderPersistence.deleteOrder(currentOrder);
        orderEventPublisher.publishEvent(OrderEvent.of(currentOrder,OrderEvent.EventType.DELETE_ORDER));
        return Optional.of(currentOrder);

    }
    
}
