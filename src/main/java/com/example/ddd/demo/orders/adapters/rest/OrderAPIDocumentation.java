package com.example.ddd.demo.orders.adapters.rest;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;
import com.example.ddd.demo.orders.application.QueryOrdersUseCase;
import com.example.ddd.demo.orders.application.command.CreateOrderCommand;
import com.example.ddd.demo.orders.application.command.UpdateOrderCommand;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.links.Link;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;

@SecurityScheme(
    name = "OIDC",
    type = SecuritySchemeType.OPENIDCONNECT,
    openIdConnectUrl = "${cas.url}/cas/oidc/.well-known/openid-configuration"
)
@Tag(name = "order", description = "The order API")
@RequestMapping(value = "/v1/order")
public interface OrderAPIDocumentation {


	/*
	 * GET ALL ORDERS
	 */

	@Operation(summary = "Get order list for current user", description = "Returns order list for current user", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
			links = {
				@Link(name = "self", operationId = "self")
		})
	})
    @GetMapping(value = "/")
    public CollectionModel<EntityModel<OrderDTO>> getAllOrdersForUser(@AuthenticationPrincipal Jwt jwt,
		@RequestParam(name="page",required = false, defaultValue = "0") int page,
		@RequestParam(name="size",required = false, defaultValue = QueryOrdersUseCase.PAGE_SIZE) int page_size);


	@Operation(summary = "Get all order list", description = "Returns all order list", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
			links = {
				@Link(name = "self", operationId = "self")
		})
	})
    @GetMapping(value = "/all")
    public CollectionModel<EntityModel<OrderDTO>> getAllOrders(@AuthenticationPrincipal Jwt jwt,
		@RequestParam(name="page",required = false, defaultValue = "0") int page,
		@RequestParam(name="size",required = false, defaultValue = QueryOrdersUseCase.PAGE_SIZE) int page_size);


	/*
	 * GET ORDER BY ID
	 */

	@Operation(summary = "Get order by id", description = "Return an order", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation",
				links = { 
					@Link(name = "self", operationId = "self"), 
					@Link(name = "cancel", operationId = "cancel") 
				}),
			@ApiResponse(responseCode = "400", description = "Invalid id supplied"),
			@ApiResponse(responseCode = "404", description = "Order not found")
	})
    @GetMapping(value = "/{id}")
    public EntityModel<OrderDTO> getOrderById(@AuthenticationPrincipal Jwt jwt,
		@PathVariable(name="id",required = true) Long id);

	/*
	 * CREATE ORDER
	 */

	@Operation(summary = "Adds a new order", description = "Add a new order", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "400", description = "Invalid data"),
			@ApiResponse(responseCode = "409", description = "Already exists") 
	})
    @PostMapping(value = "/")
    public EntityModel<OrderDTO> createOrder(@AuthenticationPrincipal Jwt jwt,
		@RequestBody(required = true) CreateOrderCommand createOrderCommand);

	/*
	 * UPDATE ORDER
	 */

	@Operation(summary = "Adds a new item to an existing order", description = "Add a new item to an existing order", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "404", description = "Order not found"),
			@ApiResponse(responseCode = "409", description = "Already exists") 
	})
    @PutMapping(value = "/{id}")
    public EntityModel<OrderDTO> updateOrder(@AuthenticationPrincipal Jwt jwt,
        @PathVariable(name="id",required = true) long id,
		@RequestBody(required = true) UpdateOrderCommand updateOrderCommand);

	/*
	 * DELETE ORDER
	 */

	@Operation(summary = "Delete existing order", description = "Delete an existing order", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "404", description = "Order not found"),
			@ApiResponse(responseCode = "409", description = "Already exists") 
	})
    @DeleteMapping(value = "/{id}")
    public EntityModel<OrderDTO> deleteOrder(@AuthenticationPrincipal Jwt jwt,
        @PathVariable(name="id",required = true) long id);

}