package com.example.ddd.demo.orders.application.command;

import java.util.List;
import java.util.Optional;

import com.example.ddd.demo.orders.application.OrderEventPublisher;
import com.example.ddd.demo.orders.application.OrderPersistence;
import com.example.ddd.demo.orders.application.RemoteUserPersistence;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.exception.OrderItemNotValidException;
import com.example.ddd.demo.orders.domain.exception.OrderNotFoundException;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;

public interface OrderCommandExecutor {
        
    default void addOrderItems(Order original, List<OrderItem> ois) throws OrderItemNotValidException {
        ois.stream().forEach(oi -> {
            try {
                original.addItem(oi);
            } catch (OrderBookNotFound e) {
                throw new OrderItemNotValidException();
            }
        });
    }



    public Optional<Order> executeCommand(
        OrderPersistence orderPersistence, 
        RemoteUserPersistence remoteUserPersistence,
        OrderBookDataResolver orderBookDataResolver,
        OrderEventPublisher orderEventPublisher) throws OrderUserNotFoundException, OrderNotFoundException;

}
