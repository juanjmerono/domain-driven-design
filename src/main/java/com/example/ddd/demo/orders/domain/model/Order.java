package com.example.ddd.demo.orders.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;

public class Order implements Serializable {
    
    private Long Id;
    private OrderNumber orderNumber;
    private Set<OrderItem> items = new HashSet<>();
    private OrderUser user;

    // Private constructor
    private Order(Long id, OrderNumber on, Set<OrderItem> items, OrderUser user) {
        this.Id = id;
        this.orderNumber = on;
        this.items = items;
        this.user = user;
    }

    // Factory method from input adapter
    public static Order of(OrderUser u) {
        return Order.of(null,OrderNumber.of(),new HashSet<>(),u);
    }
    
    // Factory method from output adapter
    public static Order of(Long id, OrderNumber on, Set<OrderItem> items, OrderUser u) {
        // Order number is not null
        Objects.requireNonNull(on,"Order number must be not null");
        Objects.requireNonNull(items,"OrderItems must be not null");
        Objects.requireNonNull(u,"User must be not null");
        return new Order(id,on,items,u);
    }

    public Set<OrderItem> getItems() {
        // Immutable set
        return Set.of(items.toArray(new OrderItem[]{}));
    }

    public void addItem(OrderItem o) throws OrderBookNotFound {
        Objects.requireNonNull(o,"Order item must be not null");
        if (!o.isValid()) throw new OrderBookNotFound();
        OrderItem newOrder = o;
        OrderItem currentItem = this.items.stream().filter(it->it.equals(o)).findFirst().orElse(null);
        if (currentItem != null) {
            this.items.remove(o);
            newOrder = OrderItem.of(currentItem, o.getAmount());
        }
        if (o.getAmount()>0) this.items.add(newOrder);
    }

    public Integer getTotalAmount() {
        return this.items.stream().map(OrderItem::getAmount).reduce(0, Integer::sum);
    }

    public BigDecimal getTotalPrice() {
        return this.items.stream().map(OrderItem::getTotalPrice).reduce(BigDecimal.ZERO, BigDecimal::add);
    }

    public Long getId() { 
        return this.Id; 
    }

    public Integer getOrderNumber() { return this.orderNumber.toInteger(); }

    public OrderUser getUser() { return this.user; }

    @Override
    public String toString() {
        return String.format("Order: %s %s %s",getId(),getOrderNumber(),getItems().stream().map(OrderItem::toString).reduce("",String::concat));
    }
}
