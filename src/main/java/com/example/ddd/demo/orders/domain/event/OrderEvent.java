package com.example.ddd.demo.orders.domain.event;

import com.example.ddd.demo.orders.domain.model.Order;

public class OrderEvent {
 
    public enum EventType {
        NEW_ORDER,
        DELETE_ORDER,
        UPDATE_ORDER,
        UPDATE_ORDER_USER
    }

    private Order target;
    private EventType type;

    public OrderEvent(Order u, EventType t) {
        this.target = u; this.type = t;
    }

    public static OrderEvent of(Order u, EventType t) {
        return new OrderEvent(u,t);
    }

    public Order getTarget() { return target; }

    @Override
    public String toString() {
        return String.format("%s: %s", this.type, this.target.toString());
    }

    public boolean isNew() { return this.type.equals(EventType.NEW_ORDER); }

    public boolean isUpdate() { return this.type.equals(EventType.UPDATE_ORDER); }

    public boolean isUpdateUser() { return this.type.equals(EventType.UPDATE_ORDER_USER); }

    public boolean isDelete() { return this.type.equals(EventType.DELETE_ORDER); }
    
}
