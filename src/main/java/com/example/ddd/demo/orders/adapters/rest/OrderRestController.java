package com.example.ddd.demo.orders.adapters.rest;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;
import com.example.ddd.demo.orders.application.CommandOrdersUseCase;
import com.example.ddd.demo.orders.application.QueryOrdersUseCase;
import com.example.ddd.demo.orders.application.command.CreateOrderCommand;
import com.example.ddd.demo.orders.application.command.DeleteOrderCommand;
import com.example.ddd.demo.orders.application.command.UpdateOrderCommand;
import com.example.ddd.demo.orders.domain.exception.OrderItemNotValidException;
import com.example.ddd.demo.orders.domain.exception.OrderNotFoundException;
import com.example.ddd.demo.orders.domain.exception.OrderUserNotFoundException;
import com.example.ddd.demo.orders.domain.model.Order;

import org.springframework.hateoas.CollectionModel;

@RestController
@ExposesResourceFor(OrderDTO.class)
public class OrderRestController implements OrderAPIDocumentation {

    @Autowired
    private OrderModelAssembler orderModelAssembler;

    @Autowired
    private QueryOrdersUseCase queryOrderUseCase;

    @Autowired
    private CommandOrdersUseCase commandOrderUseCase;

    /*
     * QUERYS
     */

    @Override
    public CollectionModel<EntityModel<OrderDTO>> getAllOrdersForUser(Jwt jwt, int page, int page_size) {
        try {
            Page<Order> pageModel = (Page<Order>)queryOrderUseCase.findAllByUser(jwt.getSubject(), page, page_size);
            return orderModelAssembler.toCollectionModel(new PageImpl<OrderDTO>(StreamSupport
                                    .stream(pageModel.spliterator(), false)
                                    .map(OrderDTO::of).collect(Collectors.toList()),
                                pageModel.getPageable(),
                                pageModel.getTotalElements()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    @PreAuthorize("hasPermission(#jwt, 'VIEW_ANY_ORDER')")
    public CollectionModel<EntityModel<OrderDTO>> getAllOrders(Jwt jwt, int page, int page_size) {
        try {
            Page<Order> pageModel = (Page<Order>)queryOrderUseCase.findAll(page, page_size);
            return orderModelAssembler.toCollectionModel(new PageImpl<OrderDTO>(StreamSupport
                                    .stream(pageModel.spliterator(), false)
                                    .map(OrderDTO::of).collect(Collectors.toList()),
                                pageModel.getPageable(),
                                pageModel.getTotalElements()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    public EntityModel<OrderDTO> getOrderById(Jwt jwt, Long id) {
        return orderModelAssembler.toModel(queryOrderUseCase.findById(id,jwt.getSubject()).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }

    /*
     * COMMANDS
     */

    @Override
    public EntityModel<OrderDTO> createOrder(Jwt jwt, CreateOrderCommand createOrderCommand) {
        try {
            Objects.requireNonNull(createOrderCommand);
            return orderModelAssembler.toModel(commandOrderUseCase
                .executeCommand(CreateOrderCommand.of(jwt.getSubject(), createOrderCommand))
                .map(OrderDTO::of).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
        } catch (NullPointerException | OrderItemNotValidException | OrderUserNotFoundException | ResponseStatusException | OrderNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    public EntityModel<OrderDTO> updateOrder(Jwt jwt, long id, UpdateOrderCommand updateOrderCommand) {
        try {
            Objects.requireNonNull(updateOrderCommand);
            return orderModelAssembler.toModel(commandOrderUseCase
                .executeCommand(UpdateOrderCommand.of(id, jwt.getSubject(), updateOrderCommand))
                .map(OrderDTO::of).orElseThrow(() -> new OrderItemNotValidException()));
        } catch (OrderNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (NullPointerException | OrderUserNotFoundException | OrderItemNotValidException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    public EntityModel<OrderDTO> deleteOrder(Jwt jwt, long id) {
        try {
            return orderModelAssembler.toModel(commandOrderUseCase
                .executeCommand(DeleteOrderCommand.of(id, jwt.getSubject()))
                .map(OrderDTO::of).orElseThrow(() -> new OrderItemNotValidException()));
        } catch (OrderNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (NullPointerException | OrderUserNotFoundException | OrderItemNotValidException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

}
