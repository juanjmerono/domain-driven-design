package com.example.ddd.demo.orders.application;

import com.example.ddd.demo.orders.adapters.rapi.catalog.RemoteBookAPI;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;

public interface RemoteBookPersistence {

    public RemoteBookAPI loadBook(String bookId) throws OrderBookNotFound;

}
