package com.example.ddd.demo.orders.domain.model;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Objects;

import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.exception.OrderItemNotValidException;

public class OrderItem implements Serializable {

    private String bookId;

    private Integer amount;

    private BigDecimal price;

    private String title;

    private boolean valid;

    // For Persistence
    OrderItem() {}

    private OrderItem(String p, Integer a, BigDecimal pr, String t, boolean valid) {
        this.bookId = p; this.amount = a; this.price = pr; this.title = t; this.valid = valid;
    }

    public static OrderItem of(OrderItem base, Integer amount) throws OrderItemNotValidException, OrderBookNotFound {
        return OrderItem.of(base.getBookId(), amount, new OrderBookDataResolver(){
            @Override
            public String getBookTitle(String bookId) { return base.getBookTitle(); }
            @Override
            public BigDecimal getBookPrice(String bookId) throws OrderBookNotFound { return base.getPrice(); }            
        });
    }

    public static OrderItem of (String bookId, Integer amount, OrderBookDataResolver resolver) {
        Objects.requireNonNull(bookId,"BookId must be not null");
        Objects.requireNonNull(amount,"Amount must be not null");
        // Amount should not be lower than zero
        if (amount<0) throw new OrderItemNotValidException();
        // Get Title From Book
        String t = "unknown";
        BigDecimal p = BigDecimal.ZERO;
        boolean valid = false;
        try {
            t = resolver.getBookTitle(bookId);
            p = resolver.getBookPrice(bookId);
            valid = true;
        } catch (OrderBookNotFound ex) {
            // The book is not valid
        }
        return new OrderItem(bookId, amount, p, t, valid);
    }

    // Total prices for this order line
    public BigDecimal getTotalPrice() {
        return this.price.multiply(BigDecimal.valueOf(this.amount));
    }

    public BigDecimal getPrice() {
        return this.price;
    }

    public String getBookId() { return this.bookId; }

    public String getBookTitle() { return this.title; }

    public Integer getAmount() { return this.amount; }

    public Boolean isValid() { return valid; }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof OrderItem) {
            OrderItem other = (OrderItem)obj;
            return Objects.equals(this.bookId, other.bookId) 
                && Objects.equals(this.price,other.price);
        }
        return false;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bookId,price);
    }

    @Override
    public String toString() {
        return String.format(" Item: %s %s %s",bookId,amount,price);
    }
}
