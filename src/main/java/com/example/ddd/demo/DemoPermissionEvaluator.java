package com.example.ddd.demo;

import java.io.Serializable;
import java.util.List;

import org.springframework.security.access.PermissionEvaluator;
import org.springframework.security.core.Authentication;
import org.springframework.security.oauth2.jwt.Jwt;

public class DemoPermissionEvaluator implements PermissionEvaluator {

    private enum PermissionHandler {
        // Only roots can create users
        CREATE_USER {
            @Override
            public boolean hasPermission(String subject, List<String> rootUsers) {
                return rootUsers.contains(subject);
            }
        },
        // Only roots can create users
        UPDATE_USER {
            @Override
            public boolean hasPermission(String subject, List<String> rootUsers) {
                return rootUsers.contains(subject);
            }
        },
        // Only roots can delete users
        DELETE_USER {
            @Override
            public boolean hasPermission(String subject, List<String> rootUsers) {
                return rootUsers.contains(subject);
            }
        },
        // Only roots can view any order
        VIEW_ANY_ORDER {
            @Override
            public boolean hasPermission(String subject, List<String> rootUsers) {
                return rootUsers.contains(subject);
            }
        };
        public abstract boolean hasPermission(String subject, List<String> rootUsers);
    }

    private List<String> rootUsers;

    public DemoPermissionEvaluator(List<String> rootUsers) {
        this.rootUsers = rootUsers;    
    }

    @Override
    public boolean hasPermission(Authentication auth, Object targetDomainObject, Object permission) {
        if ((auth == null) || (targetDomainObject == null) || !(permission instanceof String)) {
            return false;
        }
        return (targetDomainObject instanceof Jwt) ?
            hasPrivilege(auth, (Jwt)targetDomainObject, permission.toString().toUpperCase())
            : false;
    }

    @Override
    public boolean hasPermission(Authentication auth, Serializable targetId, String targetType, Object permission) {
        if ((auth == null) || (targetType == null) || !(permission instanceof String)) {
            return false;
        }
        return (auth.getPrincipal() instanceof Jwt) ? 
            hasPrivilege(auth, (Jwt)auth.getPrincipal(), permission.toString().toUpperCase())
            : false;
    }

    private boolean hasPrivilege(Authentication auth, Jwt jwt, String permission) {
        return PermissionHandler.valueOf(permission).hasPermission(jwt.getSubject(),this.rootUsers);
    }

}