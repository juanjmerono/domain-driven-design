package com.example.ddd.demo.users.application.command;

import java.util.Optional;

import com.example.ddd.demo.users.application.UsersEventPublisher;
import com.example.ddd.demo.users.application.UsersPersistence;
import com.example.ddd.demo.users.domain.event.UsersEvent;
import com.example.ddd.demo.users.domain.exception.UserNotFoundException;
import com.example.ddd.demo.users.domain.model.User;
import com.fasterxml.jackson.annotation.JsonIgnore;

import lombok.Data;

@Data
public class UpdateUserCommand implements UserCommandExecutor {

    @JsonIgnore
    private final String userId;
    private final String fullname;
    private final String email;

    public static UpdateUserCommand of(String fullname, String email) {
        // You can check things
        return new UpdateUserCommand(null, fullname, email);
    }

    public static UpdateUserCommand of(String userId, UpdateUserCommand uc) {
        // You can check things
        return new UpdateUserCommand(userId, uc.getFullname(), uc.getEmail());
    }

    @Override
    public Optional<User> executeCommand(UsersPersistence usersPersistence, UsersEventPublisher eventPublisher) throws UserNotFoundException {
        User u = usersPersistence.findById(this.getUserId()).orElseThrow(() -> new UserNotFoundException());
        u.updateFullName(this.getFullname());
        u.updateEmail(this.getEmail());
        eventPublisher.publishEvent(UsersEvent.of(u,UsersEvent.EventType.UPDATE_USER));
        return Optional.of(usersPersistence.updateUser(u));
    }
    
}
