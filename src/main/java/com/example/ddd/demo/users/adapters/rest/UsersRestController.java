package com.example.ddd.demo.users.adapters.rest;

import java.util.Objects;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;
import org.springframework.http.HttpStatus;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

import com.example.ddd.demo.users.adapters.rest.dto.UserDTO;
import com.example.ddd.demo.users.application.CommandUsersUseCase;
import com.example.ddd.demo.users.application.QueryUsersUseCase;
import com.example.ddd.demo.users.application.command.CreateUserCommand;
import com.example.ddd.demo.users.application.command.DeleteUserCommand;
import com.example.ddd.demo.users.application.command.UpdateUserCommand;
import com.example.ddd.demo.users.domain.exception.UserNotFoundException;
import com.example.ddd.demo.users.domain.exception.UserNotValidException;
import com.example.ddd.demo.users.domain.model.User;

import org.springframework.hateoas.CollectionModel;

@RestController
@ExposesResourceFor(UserDTO.class)
public class UsersRestController implements UsersAPIDocumentation {

    @Autowired
    private UsersModelAssembler userModelAssembler;

    @Autowired
    private QueryUsersUseCase queryUsersUseCase;

    @Autowired
    private CommandUsersUseCase commandUsersUseCase;


    /*
     * 
     *   QUERYS
     * 
     */

    @Override
    public CollectionModel<EntityModel<UserDTO>> getAllUsers(int page, int page_size) {
        try {
            Page<User> pageModel = (Page<User>)queryUsersUseCase.findAll(page, page_size);
            return userModelAssembler.toCollectionModel(new PageImpl<UserDTO>(StreamSupport
                                        .stream(pageModel.spliterator(), false)
                                        .map(UserDTO::of).collect(Collectors.toList()),
                                        pageModel.getPageable(),
                                        pageModel.getTotalElements()));
        } catch (IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    public EntityModel<UserDTO> getUserById(String id) {
        return userModelAssembler.toModel(queryUsersUseCase.findById(id).map(UserDTO::of).orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
    }


    /*
     * 
     *     COMMANDS
     * 
     */


    @Override
    @PreAuthorize("hasPermission(#jwt, 'CREATE_USER')")
    public EntityModel<UserDTO> createUser(Jwt jwt, CreateUserCommand createUserCommand) {
        try {
            Objects.requireNonNull(createUserCommand);
            return userModelAssembler.toModel(commandUsersUseCase
                .executeCommand(createUserCommand)
                .map(UserDTO::of)
                .orElseThrow(() -> new ResponseStatusException(HttpStatus.NOT_FOUND)));
        } catch (NullPointerException | UserNotFoundException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    @PreAuthorize("hasPermission(#jwt, 'UPDATE_USER')")
    public EntityModel<UserDTO> updateUser(Jwt jwt, String id, UpdateUserCommand updateUserCommand) {
        try {
            Objects.requireNonNull(updateUserCommand);
            return userModelAssembler.toModel(commandUsersUseCase
                .executeCommand(UpdateUserCommand.of(id,updateUserCommand))
                .map(UserDTO::of)
                .orElseThrow(() -> new UserNotValidException()));
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        } catch (NullPointerException | UserNotValidException | IllegalArgumentException e) {
            throw new ResponseStatusException(HttpStatus.NOT_ACCEPTABLE);
        }
    }

    @Override
    @PreAuthorize("hasPermission(#jwt, 'DELETE_USER')")
    public EntityModel<UserDTO> deleteUser(Jwt jwt, String id) {
        try {
            return userModelAssembler.toModel(commandUsersUseCase
                .executeCommand(DeleteUserCommand.of(id))
                .map(UserDTO::of).get());
        } catch (UserNotFoundException e) {
            throw new ResponseStatusException(HttpStatus.NOT_FOUND);
        }
    }

}
