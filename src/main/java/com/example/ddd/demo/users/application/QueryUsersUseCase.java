package com.example.ddd.demo.users.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.users.domain.model.User;

@Service
public class QueryUsersUseCase {
    
    public static final String PAGE_SIZE = "3";

    @Autowired
    private UsersPersistence usersPersistence;

    public Iterable<User> findAll(int page, int page_size) {
        return usersPersistence.findAll(page, page_size);
    }

    public Optional<User> findById(String id) {
        return usersPersistence.findById(id);
    }

}
