package com.example.ddd.demo.users.application;

import java.util.Optional;

import com.example.ddd.demo.users.domain.model.User;

public interface UsersPersistence {

    Iterable<User> findAll(int page, int page_size);

    Optional<User> findById(String id);

    User createUser(User u);

    User updateUser(User u);

    void deleteUser(User u);

}
