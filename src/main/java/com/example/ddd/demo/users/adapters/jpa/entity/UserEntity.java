package com.example.ddd.demo.users.adapters.jpa.entity;

import java.time.ZoneId;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.springframework.data.domain.AbstractAggregateRoot;

import com.example.ddd.demo.users.domain.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name="USERS")
public class UserEntity extends AbstractAggregateRoot<UserEntity> {

    @Id
    private String id;
    private String fullname;
    private String nick;
    private String email;
    private Date start;

    public User toModel() {
        return User.of(id, fullname, nick, email, start.toInstant().atZone(ZoneId.systemDefault()).toLocalDateTime());
    }

    public static UserEntity of(User u) {
        return UserEntity.builder()
            .id(u.getId())
            .fullname(u.getFullName())
            .nick(u.getNick())
            .email(u.getEmail())
            .start(Date.from(u.getStartDate().atZone(ZoneId.systemDefault()).toInstant()))
            .build();
    }

}
