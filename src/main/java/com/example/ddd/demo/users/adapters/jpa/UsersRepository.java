package com.example.ddd.demo.users.adapters.jpa;

import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import com.example.ddd.demo.users.adapters.jpa.entity.UserEntity;

@Repository
public interface UsersRepository extends PagingAndSortingRepository<UserEntity,String> {

}
