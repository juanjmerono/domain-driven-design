package com.example.ddd.demo.users.domain.event;

import com.example.ddd.demo.users.domain.model.User;

public class UsersEvent {

    public enum EventType {
        NEW_USER ,
        DELETE_USER ,
        UPDATE_USER 
    }

    private User target;
    private EventType type;

    public UsersEvent(User u, EventType t) {
        this.target = u; this.type = t;
    }

    public static UsersEvent of(User u, EventType t) {
        return new UsersEvent(u,t);
    }

    public User getTarget() { return target; }

    @Override
    public String toString() {
        return String.format("%s: %s", this.type, this.target.toString());
    }

    public boolean isNew() { return this.type.equals(EventType.NEW_USER); }

    public boolean isUpdate() { return this.type.equals(EventType.UPDATE_USER); }

    public boolean isDelete() { return this.type.equals(EventType.DELETE_USER); }

}
