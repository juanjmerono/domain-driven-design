package com.example.ddd.demo.users.application.command;

import java.util.Optional;

import com.example.ddd.demo.users.application.UsersEventPublisher;
import com.example.ddd.demo.users.application.UsersPersistence;
import com.example.ddd.demo.users.domain.event.UsersEvent;
import com.example.ddd.demo.users.domain.exception.UserNotFoundException;
import com.example.ddd.demo.users.domain.model.User;

import lombok.Data;

@Data
public class DeleteUserCommand implements UserCommandExecutor {

    private final String userId;

    public static DeleteUserCommand of(String userId) {
        // You can check things
        return new DeleteUserCommand(userId);
    }

    @Override
    public Optional<User> executeCommand(UsersPersistence usersPersistence, UsersEventPublisher eventPublisher) throws UserNotFoundException {
        User u = usersPersistence.findById(this.getUserId()).orElseThrow(() -> new UserNotFoundException());
        usersPersistence.deleteUser(u);
        eventPublisher.publishEvent(UsersEvent.of(u,UsersEvent.EventType.DELETE_USER));
        return Optional.of(u);
    }
    
}
