package com.example.ddd.demo.users.application;

import com.example.ddd.demo.users.domain.event.UsersEvent;

public interface UsersEventPublisher {
    
    public void publishEvent(UsersEvent ue);

}
