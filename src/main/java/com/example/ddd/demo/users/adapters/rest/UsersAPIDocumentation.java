package com.example.ddd.demo.users.adapters.rest;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.security.core.annotation.AuthenticationPrincipal;
import org.springframework.security.oauth2.jwt.Jwt;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.ddd.demo.users.adapters.rest.dto.UserDTO;
import com.example.ddd.demo.users.application.QueryUsersUseCase;
import com.example.ddd.demo.users.application.command.CreateUserCommand;
import com.example.ddd.demo.users.application.command.UpdateUserCommand;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityScheme;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.enums.SecuritySchemeType;
import io.swagger.v3.oas.annotations.links.Link;

@SecurityScheme(
    name = "OIDC",
    type = SecuritySchemeType.OPENIDCONNECT,
    openIdConnectUrl = "${cas.url}/cas/oidc/.well-known/openid-configuration"
)
@Tag(name = "user", description = "The user API")
@RequestMapping(value = "/v1/user")
public interface UsersAPIDocumentation {


	/*
	 * GET ALL USERS
	 */

	@Operation(summary = "Get user list", description = "Returns user list", 
		responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
			links = {
				@Link(name = "self", operationId = "self")
		})
	})
    @GetMapping(value = "/")
    public CollectionModel<EntityModel<UserDTO>> getAllUsers(
		@RequestParam(name="page",required = false, defaultValue = "0") int page,
		@RequestParam(name="size",required = false, defaultValue = QueryUsersUseCase.PAGE_SIZE) int page_size);


	/*
	 * GET USER BY ID
	 */

	@Operation(summary = "Get order by id", description = "Return an order", responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
				links = { 
					@Link(name = "self", operationId = "self"), 
					@Link(name = "cancel", operationId = "cancel") 
				}),
		@ApiResponse(responseCode = "400", description = "Invalid id supplied"),
		@ApiResponse(responseCode = "404", description = "Order not found")
	})
    @GetMapping(value = "/{id}")
    public EntityModel<UserDTO> getUserById(@PathVariable(name="id",required = true) String id);

	/*
	 * CREATE USER
	 */

	@Operation(summary = "Adds a new user", description = "Add a new user", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "400", description = "Invalid data"),
			@ApiResponse(responseCode = "401", description = "Unauthorized") 
	})
    @PostMapping(value = "/")
    public EntityModel<UserDTO> createUser(@AuthenticationPrincipal Jwt jwt,
					@RequestBody(required = true) CreateUserCommand createUserCommand);

	/*
	 * UPDATE USER
	 */

	@Operation(summary = "Update data of existing user", description = "Update data of existing user", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "404", description = "User not found"),
			@ApiResponse(responseCode = "401", description = "Unauthorized") 
	})
    @PutMapping(value = "/{id}")
    public EntityModel<UserDTO> updateUser(@AuthenticationPrincipal Jwt jwt,
        @PathVariable(name="id",required = true) String id, 
		@RequestBody(required = true) UpdateUserCommand updateUserCommand);


	/*
	 * DELETE USER
	 */

	@Operation(summary = "Delete existing user", description = "Delete existing user", 
		security = { @SecurityRequirement(name = "OIDC", scopes = "openid") },
		responses = {
			@ApiResponse(responseCode = "200", description = "Successful operation"),
			@ApiResponse(responseCode = "404", description = "User not found"),
			@ApiResponse(responseCode = "401", description = "Unauthorized") 
	})
	@DeleteMapping(value = "/{id}")
	public EntityModel<UserDTO> deleteUser(@AuthenticationPrincipal Jwt jwt,
		@PathVariable(name="id",required = true) String id);

	}