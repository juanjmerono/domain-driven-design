package com.example.ddd.demo.users.adapters.rest;

import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.example.ddd.demo.users.adapters.rest.dto.UserDTO;

import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;
import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.methodOn;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;

@Component
public class UsersModelAssembler implements RepresentationModelAssembler<UserDTO,EntityModel<UserDTO>>{

    @Autowired
    private PagedResourcesAssembler<UserDTO> pagedResourcesAssembler;

    @Override
    public EntityModel<UserDTO> toModel(UserDTO entity) {
        return EntityModel.of(entity,
            linkTo(UsersRestController.class).slash(entity.getId()).withSelfRel(),
            linkTo(UsersRestController.class).slash(entity.getId()).withRel("put"),
            linkTo(UsersRestController.class).slash(entity.getId()).withRel("delete")
        );
    }

    @Override
    public CollectionModel<EntityModel<UserDTO>> toCollectionModel(Iterable<? extends UserDTO> entities) {
        PagedModel<EntityModel<UserDTO>> model = pagedResourcesAssembler.toModel((Page)entities, this);
        model.add(linkTo(methodOn(UsersRestController.class).createUser(null, null)).withRel("post"));
        return model;    
    }

}
