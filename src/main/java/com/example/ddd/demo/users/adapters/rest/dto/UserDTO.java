package com.example.ddd.demo.users.adapters.rest.dto;

import com.example.ddd.demo.users.domain.model.User;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserDTO {

    private String id;
    private String nick;
    private String fullname;
    private String email;

    public static UserDTO of(User o) {
        return UserDTO.builder()
            .id(o.getId())
            .fullname(o.getFullName())
            .nick(o.getNick())
            .email(o.getEmail())
            .build();
    }

}
