package com.example.ddd.demo.users.application.command;

import java.util.Optional;

import com.example.ddd.demo.users.application.UsersEventPublisher;
import com.example.ddd.demo.users.application.UsersPersistence;
import com.example.ddd.demo.users.domain.event.UsersEvent;
import com.example.ddd.demo.users.domain.model.User;

import lombok.Data;

@Data
public class CreateUserCommand implements UserCommandExecutor {

    private final String fullname;
    private final String nickname;
    private final String email;

    public static CreateUserCommand of(String fullname, String nickname, String email) {
        // You can check things
        return new CreateUserCommand(fullname, nickname, email);
    }

    @Override
    public Optional<User> executeCommand(UsersPersistence usersPersistence, UsersEventPublisher eventPublisher) {
        User u = usersPersistence.createUser(User.of(this.getFullname(), 
                                                    this.getNickname(), 
                                                    this.getEmail()));
        eventPublisher.publishEvent(UsersEvent.of(u,UsersEvent.EventType.NEW_USER));
        return Optional.of(u);
    }
    
}
