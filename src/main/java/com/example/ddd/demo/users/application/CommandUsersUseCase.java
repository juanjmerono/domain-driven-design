package com.example.ddd.demo.users.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.users.application.command.UserCommandExecutor;
import com.example.ddd.demo.users.domain.exception.UserNotFoundException;
import com.example.ddd.demo.users.domain.model.User;

@Service
public class CommandUsersUseCase {
    
    @Autowired
    private UsersPersistence usersPersistence;

    @Autowired
    private UsersEventPublisher usersEventPublisher;

    public Optional<User> executeCommand(UserCommandExecutor userCommand) throws UserNotFoundException, IllegalArgumentException {
        return userCommand.executeCommand(usersPersistence,usersEventPublisher);
    }
    
}
