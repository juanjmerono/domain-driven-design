package com.example.ddd.demo.users.adapters.publisher;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.users.application.UsersEventPublisher;
import com.example.ddd.demo.users.domain.event.UsersEvent;

@Service
public class UsersEventPublisherImpl implements UsersEventPublisher {

    @Autowired
    private ApplicationEventPublisher eventPublisher;

    @Override
    public void publishEvent(UsersEvent ue) {
        eventPublisher.publishEvent(ue);
    }
    
}
