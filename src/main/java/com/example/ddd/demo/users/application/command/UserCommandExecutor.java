package com.example.ddd.demo.users.application.command;

import java.util.Optional;

import com.example.ddd.demo.users.application.UsersEventPublisher;
import com.example.ddd.demo.users.application.UsersPersistence;
import com.example.ddd.demo.users.domain.exception.UserNotFoundException;
import com.example.ddd.demo.users.domain.model.User;

public interface UserCommandExecutor {

    public Optional<User> executeCommand(UsersPersistence usersPersistence, UsersEventPublisher eventPublisher) throws UserNotFoundException;
    
}
