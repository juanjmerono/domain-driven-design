package com.example.ddd.demo.users.domain.model;

import java.time.LocalDateTime;
import java.util.Objects;
import java.util.UUID;

public class User {
    
    private static int MIN_NICK_LENGTH = 6;
    private static int UUID_LENGTH = 36;

    private String id;
    private String fullname;
    private String nick;
    private String email;
    private LocalDateTime startDate;

    private User(String id, String fullname, String nick, String email, LocalDateTime date) {
        this.id = id;
        this.fullname = fullname;
        this.nick = nick;
        this.email = email;
        this.startDate = date;
    }

    public static User of(String id, String fullname, String nick, String email, LocalDateTime date) {
        Objects.requireNonNull(id);
        if (id.length()<UUID_LENGTH) throw new IllegalArgumentException();
        Objects.requireNonNull(fullname);
        Objects.requireNonNull(nick);
        if (nick.length()<MIN_NICK_LENGTH) throw new IllegalArgumentException();
        Objects.requireNonNull(date);
        Objects.requireNonNull(email);
        if (email.chars().filter(c->c=='@').count()!=1) throw new IllegalArgumentException();
        return new User(id, fullname, nick, email, date);
    }

    public static User of(String fullname, String nick, String email) {
        return User.of(UUID.randomUUID().toString(),fullname,nick,email,LocalDateTime.now());
    }

    public String getId() { return id; }

    public String getFullName() { return fullname; }

    public String getNick() { return nick; }

    public String getEmail() { return email; }

    public LocalDateTime getStartDate() { return startDate; }

    public void updateFullName(String fn) { 
        Objects.requireNonNull(fn);
        this.fullname = fn; 
    }

    public void updateEmail(String email) { 
        Objects.requireNonNull(email);
        if (email.chars().filter(c->c=='@').count()!=1) throw new IllegalArgumentException();
        this.email = email; 
    }

}
