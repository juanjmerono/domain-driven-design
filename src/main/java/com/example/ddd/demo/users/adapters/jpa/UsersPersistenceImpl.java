package com.example.ddd.demo.users.adapters.jpa;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.users.adapters.jpa.entity.UserEntity;
import com.example.ddd.demo.users.application.UsersPersistence;
import com.example.ddd.demo.users.domain.model.User;

@Service
public class UsersPersistenceImpl implements UsersPersistence {

    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Iterable<User> findAll(int page, int page_size) {
        return usersRepository.findAll(PageRequest.of(page, page_size)).map(UserEntity::toModel);
    }

    @Override
    public Optional<User> findById(String id) {
        return usersRepository.findById(id).map(UserEntity::toModel);
    }

    private User saveUser(User u) {
        return usersRepository.save(UserEntity.of(u)).toModel();
    }

    @Override
    public User createUser(User u) {
        return usersRepository.save(UserEntity.of(u)).toModel();
    }

    @Override
    public User updateUser(User u) {
        return saveUser(u);
    }

    @Override
    public void deleteUser(User u) {
        usersRepository.delete(UserEntity.of(u));
    }

}
