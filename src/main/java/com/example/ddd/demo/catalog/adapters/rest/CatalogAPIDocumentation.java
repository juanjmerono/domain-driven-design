package com.example.ddd.demo.catalog.adapters.rest;

import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.ddd.demo.catalog.adapters.rest.dto.BookDTO;

import io.swagger.v3.oas.annotations.OpenAPIDefinition;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.info.Info;
import io.swagger.v3.oas.annotations.tags.Tag;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.links.Link;

@OpenAPIDefinition (
    info = @Info(title = "Domain Driven Design API", version = "${server.image:0.0.0}", description = "This is a domain driven design API for testing endpoints.")
)
@Tag(name = "catalog", description = "The catalog API")
@RequestMapping("/v1/catalog")
public interface CatalogAPIDocumentation {

	@Operation(summary = "Get order list", description = "Returns order list", responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
			links = {
				@Link(name = "self", operationId = "self"), 
		})
	})
    @GetMapping(value="/", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    public CollectionModel<EntityModel<BookDTO>> loadBooksByPage(@RequestParam(name = "page", required = false, defaultValue = "0") int page);
    
	@Operation(summary = "Get book by id", description = "Return a book", responses = {
		@ApiResponse(responseCode = "200", description = "Successful operation",
				links = { 
					@Link(name = "self", operationId = "self") 
				}),
		@ApiResponse(responseCode = "400", description = "Invalid id supplied"),
		@ApiResponse(responseCode = "404", description = "Order not found")
	})
    @GetMapping(value="/{id}", produces = {MediaType.APPLICATION_JSON_VALUE, MediaTypes.HAL_JSON_VALUE})
    public EntityModel<BookDTO> loadBookById(@PathVariable(name = "id", required = true) int bookId);

}
