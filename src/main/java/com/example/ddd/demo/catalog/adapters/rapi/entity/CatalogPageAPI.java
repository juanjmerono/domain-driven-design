package com.example.ddd.demo.catalog.adapters.rapi.entity;

import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;

import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import com.example.ddd.demo.catalog.domain.models.Book;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true, value = {"mockitoInterceptor"})
public class CatalogPageAPI {

    public static int PAGE_SIZE = 32;

    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private int count;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private List<BookAPI> results;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String next;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private String previous;

    private int getCurrentPage() {
        if (this.previous!=null) {
            Pattern r = Pattern.compile(".*page=(\\d+).*");
            Matcher m = r.matcher(this.previous);
            // No page in previous link so previous is default page 1
            return m.find()?Integer.parseInt(m.group(1)):0;
        }
        // No previous page page 1
        return 0;
    }

    public Iterable<Book> toModelList() {
        return new PageImpl<Book>(
            results.stream().map(BookAPI::toModel).collect(Collectors.toList()),
            Pageable.ofSize(PAGE_SIZE).withPage(getCurrentPage()),
            count);
    }

    public Book toModel() {
        // Return the first result as supposed to be only one
        return results.get(0).toModel();
    }

}
