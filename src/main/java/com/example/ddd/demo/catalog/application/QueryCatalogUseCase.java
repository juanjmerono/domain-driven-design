package com.example.ddd.demo.catalog.application;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.ddd.demo.catalog.domain.models.Book;

@Service
public class QueryCatalogUseCase {
    
    @Autowired
    private CatalogPersistence catalogPersistence;

    public Iterable<Book> loadBooksByPage(int page) {
        return catalogPersistence.loadBooksByPage(page);
    }

    public Optional<Book> loadBookById(int bookId) {
        return catalogPersistence.loadBookById(bookId);
    }

}
