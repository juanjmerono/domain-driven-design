package com.example.ddd.demo.catalog.adapters.rest.dto;

import java.math.BigDecimal;
import java.util.List;
import java.util.stream.Collectors;

import com.example.ddd.demo.catalog.domain.models.Book;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class BookDTO {
    private int id;
    private String title;
    private List<AuthorDTO> authors;
    private String urlCover;
    private String urlDownload;
    private long downloads;
    private BigDecimal price;

    public static BookDTO of (Book o) {
        return BookDTO.builder()
            .id(o.getId())
            .title(o.getTitle())
            .authors(o.getAuthors().stream().map(AuthorDTO::of).collect(Collectors.toList()))
            .urlCover(o.getCoverUrl())
            .urlDownload(o.getDownloadLink())
            .downloads(o.getDownloadCount())
            .price(o.getPrice())
            .build();
    }

}
