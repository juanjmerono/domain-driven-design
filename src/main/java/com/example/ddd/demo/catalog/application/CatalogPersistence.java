package com.example.ddd.demo.catalog.application;

import java.util.Optional;

import com.example.ddd.demo.catalog.domain.models.Book;

public interface CatalogPersistence {
    
    public Iterable<Book> loadBooksByPage(int page);
    public Optional<Book> loadBookById(int bookId);

}
