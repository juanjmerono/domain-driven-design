package com.example.ddd.demo.catalog.domain.models;

import java.util.Objects;

public class Author {
    
    private String name;
    private int birthYear;
    private int deathYear;

    private Author(String name, int byear, int dyear) {
        this.name = name; this.birthYear = byear; this.deathYear = dyear;
    }

    public static Author of(String name, int byear, int dyear) {
        Objects.requireNonNull(name);
        return new Author(name, byear, dyear);
    }

    public String getName() { return this.name; }

    public int getBirthYear() { return this.birthYear; }

    public int getDathYear() { return this.deathYear; }

}
