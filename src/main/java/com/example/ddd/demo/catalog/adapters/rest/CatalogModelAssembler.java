package com.example.ddd.demo.catalog.adapters.rest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.RepresentationModelAssembler;
import org.springframework.stereotype.Component;

import com.example.ddd.demo.catalog.adapters.rest.dto.BookDTO;

import static org.springframework.hateoas.server.mvc.WebMvcLinkBuilder.linkTo;

@Component
public class CatalogModelAssembler implements RepresentationModelAssembler<BookDTO,EntityModel<BookDTO>>{

    @Autowired
    private PagedResourcesAssembler<BookDTO> pagedResourcesAssembler;

    @Override
    public EntityModel<BookDTO> toModel(BookDTO entity) {
        return EntityModel.of(entity,
            linkTo(CatalogRestController.class).slash(entity.getId()).withSelfRel()
        );
    }

    @Override
    public CollectionModel<EntityModel<BookDTO>> toCollectionModel(Iterable<? extends BookDTO> entities) {
        return pagedResourcesAssembler.toModel((Page)entities, this);
    }

}
