package com.example.ddd.demo.catalog.adapters.rest.dto;

import com.example.ddd.demo.catalog.domain.models.Author;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class AuthorDTO {
    private String name;  
    private int birthYear;
    private int deathYear;  

    public static AuthorDTO of (Author a) {
        return AuthorDTO.builder()
            .name(a.getName())
            .birthYear(a.getBirthYear())
            .deathYear(a.getDathYear())
            .build();
    }
}
