package com.example.ddd.demo.catalog.adapters.rapi.entity;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import com.example.ddd.demo.catalog.domain.models.Book;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonProperty;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties(ignoreUnknown = true, value = {"mockitoInterceptor"})
public class BookAPI {
    
    private int id;
    private String title;
    private List<AuthorAPI> authors;
    @JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
    private Map<String,String> formats;
    @JsonProperty(value = "download_count",access = JsonProperty.Access.WRITE_ONLY)
    private long downloads;

    public Book toModel() {
        return Book.of(this.id,
            this.title,
            this.authors.stream().map(AuthorAPI::toModel).collect(Collectors.toList()),
            this.formats.get("image/jpeg"),
            this.formats.get("text/html"),
            this.downloads);
    }

}
