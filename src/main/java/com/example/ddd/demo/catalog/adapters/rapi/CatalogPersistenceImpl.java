package com.example.ddd.demo.catalog.adapters.rapi;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.example.ddd.demo.catalog.adapters.rapi.entity.CatalogPageAPI;
import com.example.ddd.demo.catalog.application.CatalogPersistence;
import com.example.ddd.demo.catalog.domain.models.Book;

@Service
public class CatalogPersistenceImpl implements CatalogPersistence {

    @Autowired
    private RestTemplate restTemplate;
    
    @Value("${gutendex.api.url}")
    private String apiGutendex;

    public Iterable<Book> loadBooksByPage(int page) {
        try {
            String apiQuery = apiGutendex+"/?page="+(page+1);
            return restTemplate
                .getForEntity(apiQuery, CatalogPageAPI.class)
                .getBody().toModelList();
        } catch (NotFound nfe) {
            return null;
        }
    }

    public Optional<Book> loadBookById(int bookId) {
        try {
            String apiQuery = apiGutendex+"/?ids="+bookId;
            return Optional.of(restTemplate
                .getForEntity(apiQuery, CatalogPageAPI.class)
                .getBody().toModel());
        } catch (NotFound nfe) {
            return null;
        }
    }
    
}
