package com.example.ddd.demo.catalog.domain.models;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

public class Book {
    
    private int id;
    private String title;
    private List<Author> authors;
    private String urlCover;
    private String urlDownload;
    private long downloads;

    private Book(int id, String title, List<Author> auths, String urlCover, String urlDownload, long downloads) {
        this.id = id;
        this.title = title;
        this.authors = auths;
        this.urlCover = urlCover;
        this.urlDownload = urlDownload;
        this.downloads = downloads;
    }

    public static Book of(int id, String title, List<Author> auths, String urlCover, String urlDownload, long downloads) {
        Objects.requireNonNull(title,"Title must be not null");
        Objects.requireNonNull(auths,"Author must be not null");
        Objects.requireNonNull(urlCover,"Cover must be not null");
        Objects.requireNonNull(urlDownload,"Download url must be not null");
        return new Book(id, title, auths, urlCover, urlDownload, downloads);
    }

    public int getId() { return this.id; }

    public String getTitle() { return this.title; }

    public List<Author> getAuthors() { 
        // Immutable list
        return List.of(this.authors.toArray(new Author[]{})); 
    }

    public String getCoverUrl() {
        return this.urlCover;
    }

    public String getDownloadLink() {
        return this.urlDownload;
    }

    public long getDownloadCount() { 
        return this.downloads; 
    }

    public BigDecimal getPrice() {
        // Prices is related to download count
        return BigDecimal.valueOf(this.getDownloadCount())
            .divide(BigDecimal.TEN.multiply(BigDecimal.TEN))
            .setScale(2);
    }
}
