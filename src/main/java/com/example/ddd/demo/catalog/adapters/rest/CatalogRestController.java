package com.example.ddd.demo.catalog.adapters.rest;

import org.springframework.web.bind.annotation.RestController;

import com.example.ddd.demo.catalog.adapters.rest.dto.BookDTO;
import com.example.ddd.demo.catalog.application.QueryCatalogUseCase;
import com.example.ddd.demo.catalog.domain.models.Book;

import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.hateoas.CollectionModel;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.server.ExposesResourceFor;

@RestController
@ExposesResourceFor(BookDTO.class)
public class CatalogRestController implements CatalogAPIDocumentation {

    @Autowired
    private CatalogModelAssembler catalogModelAssembler;

    @Autowired
    private QueryCatalogUseCase queryCatalogUseCase;
    
    @Override
    public CollectionModel<EntityModel<BookDTO>> loadBooksByPage(int page) {
        Page<Book> p = (Page<Book>)queryCatalogUseCase.loadBooksByPage(page);
        return catalogModelAssembler.toCollectionModel(new PageImpl<BookDTO>(StreamSupport
            .stream(queryCatalogUseCase.loadBooksByPage(page).spliterator(), false)
            .map(BookDTO::of).collect(Collectors.toList()),
            p.getPageable(),
            p.getTotalElements()));
    }
    
    @Override
    public EntityModel<BookDTO> loadBookById(int bookId) {
        return catalogModelAssembler.toModel(queryCatalogUseCase.loadBookById(bookId).map(BookDTO::of).get());
    }

}
