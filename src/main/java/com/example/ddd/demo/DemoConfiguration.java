package com.example.ddd.demo;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityCustomizer;
import org.springframework.security.web.SecurityFilterChain;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@Configuration
public class DemoConfiguration implements WebMvcConfigurer {
    
    @Value("${server.scopes}")
    private String myServerScopes;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }
    
    @Bean
    public SecurityFilterChain filterChain(HttpSecurity http) throws Exception {
        http
            .csrf().disable()
            .authorizeHttpRequests((authz) -> authz
                .anyRequest().hasAnyAuthority(myServerScopes)
            )
            .oauth2ResourceServer()
            .jwt();
        return http.build();
    }
    
    @Bean
    public WebSecurityCustomizer webSecurityCustomizer() {
        // Ignore public access paths
        return (web) -> web.ignoring()
            .antMatchers(
                "/swagger-ui.html", 
                "/swagger-ui/**",
                "/api-docs/**",
                "/h2-console/**",
                "/v1/catalog/**"
            )
            .antMatchers(HttpMethod.GET, 
                "/v1/user/**"
            );
    }

}
