package com.example.ddd.demo.orders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotEquals;

import java.math.BigDecimal;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.example.ddd.demo.orders.adapters.rapi.user.RemoteUserAPI;
import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;
import com.example.ddd.demo.orders.application.command.CreateOrderCommand;
import com.example.ddd.demo.orders.application.command.UpdateOrderCommand;
import com.example.ddd.demo.orders.application.command.UpdateOrderItem;
import com.example.ddd.demo.orders.domain.event.OrderEvent;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.example.ddd.demo.orders.OrderRestQueryUseCaseTests.BASE_LINK_FORMAT;
import static com.example.ddd.demo.orders.OrderRestQueryUseCaseTests.BASE_PATH;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
@AutoConfigureMockMvc
@Transactional
public class OrderRestCommandUseCaseTests {

    public static String VALID_SCOPE = "SCOPE_micampus";
    public static String MY_TEST_USER = "51775fd2-fca6-11ec-b939-0242ac120001";
    public static String MY_TEST_USER2 = "51775fd2-fca6-11ec-b939-0242ac120002";
    public static String MY_ADMIN_USER = "51775fd2-fca6-11ec-b939-0242ac120009";

    @Value("${users.api.url}")
    private String apiUsers;

    @MockBean
    private RestTemplate restTemplateTest;

    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @Autowired 
    private ApplicationEvents applicationEvents;

    @MockBean
    private OrderBookDataResolver bookDataResolver;

    @BeforeEach
    public void setup() throws OrderBookNotFound {
        Mockito.when(bookDataResolver.getBookTitle("book001")).thenReturn("bookTitle001");
        Mockito.when(bookDataResolver.getBookPrice("book001")).thenReturn(BigDecimal.valueOf(1.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book002")).thenReturn("bookTitle002");
        Mockito.when(bookDataResolver.getBookPrice("book002")).thenReturn(BigDecimal.valueOf(2.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book003")).thenReturn("bookTitle003");
        Mockito.when(bookDataResolver.getBookPrice("book003")).thenReturn(BigDecimal.valueOf(10.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book005")).thenThrow(OrderBookNotFound.class);
        Mockito.when(bookDataResolver.getBookPrice("book005")).thenThrow(OrderBookNotFound.class);
        ResponseEntity<RemoteUserAPI> responseEntity = (ResponseEntity<RemoteUserAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntity.getBody()).thenReturn(RemoteUserAPI.builder()
            .email("email@um.es")
            .nick("minickname")
            .id(MY_TEST_USER2)
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiUsers + "/" + MY_TEST_USER2, RemoteUserAPI.class))
            .thenReturn(responseEntity);
        /*Mockito.when(restTemplateTest.getForEntity(apiUsers + "/" + USER_ID3, RemoteUserAPI.class))
            .thenThrow(NotFound.class);*/
        objectMapper.registerModule(new Jackson2HalModule());
    }


    private void checkHateoasLinks(EntityModel<OrderDTO> entity) {
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("put").get().getHref());
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("delete").get().getHref());
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("self").get().getHref());
    }

    @Test
    public void test_should_call_users_v1() {
        assertEquals("/v1/user", apiUsers.substring(apiUsers.indexOf("/v1/user")));
    }

    /*
     * CREATE METHODS
     */

    @Test
    public void test_create_new_empty_order_should_be_400() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void test_create_no_json_order_should_not_be_ok() throws Exception {
        String requestJson = "{ \"invent\": 10 }";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    private Long createOrder(String product, int count) throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateOrderCommand.of(product,count));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER2))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<OrderDTO> orderTest = objectMapper.readValue(content, new TypeReference<EntityModel<OrderDTO>>(){});
        Long id = orderTest.getContent().getId();
        assertEquals("email2@a.com", orderTest.getContent().getUserEmail());
        assertEquals(count, orderTest.getContent().getTotalAmount());
        assertEquals(1, orderTest.getContent().getItems().size());
        assertEquals(BigDecimal.valueOf(Math.round(1.2*count*100)/100.0).setScale(2), orderTest.getContent().getTotalPrice());
        checkHateoasLinks(orderTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(OrderEvent.class)
                .filter(event -> event.getTarget().getItems().size() == 1
                                && event.getTarget().getTotalAmount() == count
                                && BigDecimal.valueOf(Math.round(1.2*count*100)/100.0).setScale(2).equals(event.getTarget().getTotalPrice())
                                && event.isNew())
                .count());
        return id;
    }

    @Test
    public void test_create_new_order_should_be_ok() throws Exception {
        createOrder("book001", 3);
    }

    @Test
    public void test_create_consecutive_new_order_should_be_ok() throws Exception {
        Long id1 = createOrder("book001", 3);
        Long id2 = createOrder("book001", 2);
        assertNotEquals(id1,id2);
        Mockito.verify(restTemplateTest,Mockito.times(1)).getForEntity(Mockito.anyString(),Mockito.any());
    }

    @Test
    public void test_create_new_invalid_order_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateOrderCommand.of("book001",-7));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    /*
     * UPDATE METHODS
     */

    @Test
    public void test_update_order_missing_book_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateOrderCommand.of("book005",2));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"100")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_update_with_invalid_order_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateOrderCommand.of("book004",-7));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"100")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_update_no_order_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateOrderCommand.of("book004",1));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"999")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_update_no_json_order_should_not_be_ok() throws Exception {
        String requestJson = "{ \"invent\": 10 }";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"999")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_update_order_should_be_ok() throws Exception {
        UpdateOrderItem [] items = new UpdateOrderItem[]{
            UpdateOrderItem.of("book001",0),
            UpdateOrderItem.of("book002",3)};
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateOrderCommand.of(items));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"100")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<OrderDTO> orderTest = objectMapper.readValue(content, new TypeReference<EntityModel<OrderDTO>>(){});
        assertEquals(100L, orderTest.getContent().getId());
        assertEquals(4, orderTest.getContent().getTotalAmount());
        assertEquals(2, orderTest.getContent().getItems().size());
        assertEquals(BigDecimal.valueOf(16.8).setScale(2), orderTest.getContent().getTotalPrice());
        checkHateoasLinks(orderTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(OrderEvent.class)
                .filter(event -> event.getTarget().getItems().size() == 2
                                && event.getTarget().getTotalAmount() == 4
                                && BigDecimal.valueOf(16.8).setScale(2).equals(event.getTarget().getTotalPrice())
                                && event.isUpdate())
                .count());
    }

    /*
     * DELETE METHODS
     */

    @Test
    public void test_delete_order_no_order_should_not_be_supported() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(405, status);
    }

    @Test
    public void test_delete_missing_order_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"999")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_delete_order_no_user_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"100")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_delete_order_not_owned_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"100")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER2))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_delete_order_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"100")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<OrderDTO> orderTest = objectMapper.readValue(content, new TypeReference<EntityModel<OrderDTO>>(){});
        assertEquals(100L, orderTest.getContent().getId());
        assertEquals(4, orderTest.getContent().getTotalAmount());
        assertEquals(3, orderTest.getContent().getItems().size());
        assertEquals(BigDecimal.valueOf(15.8).setScale(2), orderTest.getContent().getTotalPrice());
        checkHateoasLinks(orderTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(OrderEvent.class)
                .filter(event -> event.getTarget().getItems().size() == 3
                                && event.getTarget().getTotalAmount() == 4
                                && BigDecimal.valueOf(15.8).setScale(2).equals(event.getTarget().getTotalPrice())
                                && event.isDelete())
                .count());
    }

}
