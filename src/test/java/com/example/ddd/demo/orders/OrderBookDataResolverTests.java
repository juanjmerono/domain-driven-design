package com.example.ddd.demo.orders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.util.concurrent.TimeUnit;

import org.awaitility.Awaitility;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.client.HttpClientErrorException.NotFound;

import com.example.ddd.demo.orders.adapters.rapi.catalog.RemoteBookAPI;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;
import com.example.ddd.demo.orders.domain.model.OrderUser;

import static com.example.ddd.demo.orders.OrderModelTests.USER_ID1;
import static com.example.ddd.demo.orders.OrderModelTests.USER_NICK1;
import static com.example.ddd.demo.orders.OrderModelTests.USER_EMAIL1;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class OrderBookDataResolverTests {

    private static String BOOK_ID1 = "1342";
    private static String BOOK_TITLE1 = "remote book title";
    private static String BOOK_ID404 = "0000";

    @MockBean
    private RestTemplate restTemplateTest;
    
    @Autowired
    private OrderBookDataResolver bookDataResolver;

    @Value("${catalog.api.url}")
    private String apiCatalog;

    @BeforeEach
    public void setup() {
        ResponseEntity<RemoteBookAPI> responseEntity = (ResponseEntity<RemoteBookAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntity.getBody()).thenReturn(RemoteBookAPI.builder()
            .id(BOOK_ID1)
            .title(BOOK_TITLE1)
            .price(BigDecimal.valueOf(15.1).setScale(2))
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiCatalog + "/" + BOOK_ID1, RemoteBookAPI.class))
            .thenReturn(responseEntity);
        Mockito.when(restTemplateTest.getForEntity(apiCatalog + "/" + BOOK_ID404, RemoteBookAPI.class))
            .thenThrow(NotFound.class);
    }

    @Test
    public void test_should_call_catalog_v1() {
        assertEquals("/v1/catalog", apiCatalog.substring(apiCatalog.indexOf("/v1/catalog")));
    }
    
    @Test
    public void test_same_order_not_duplicate_search() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of(BOOK_ID1,1,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        testOrder.addItem(OrderItem.of(BOOK_ID1,2,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        assertEquals(2,testOrder.getTotalAmount());
        assertEquals(BOOK_TITLE1, testOrder.getItems().stream().findAny().get().getBookTitle());
        assertEquals(BigDecimal.valueOf(15.1).setScale(2), testOrder.getItems().stream().findAny().get().getPrice());
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
        Mockito.verify(restTemplateTest,Mockito.times(1)).getForEntity(Mockito.anyString(),Mockito.any());
    }

    @Test
    public void test_cache_evicted() {
        Awaitility.await()
            .atMost(5,TimeUnit.SECONDS)
            .untilAsserted(() -> { 
                assertEquals(BOOK_TITLE1,OrderItem.of(BOOK_ID1,1,bookDataResolver).getBookTitle());
                Mockito.verify(restTemplateTest,Mockito.times(1)).getForEntity(Mockito.anyString(),Mockito.any()); 
            });

    }

}
