package com.example.ddd.demo.orders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.junit.jupiter.api.Assertions.assertTrue;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.TestPropertySource;

import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.model.Order;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.example.ddd.demo.orders.domain.model.OrderItem;
import com.example.ddd.demo.orders.domain.model.OrderNumber;
import com.example.ddd.demo.orders.domain.model.OrderUser;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class OrderModelTests {

    public static String USER_ID1 = "51775fd2-fca6-11ec-b939-0242ac120001";
    public static String USER_EMAIL1 = "email1@a.com";
    public static String USER_NICK1 = "nickjdo1";

    @MockBean
    private OrderBookDataResolver bookDataResolver;

    @BeforeEach
    public void setup() throws OrderBookNotFound {
        Mockito.when(bookDataResolver.getBookTitle("book")).thenReturn("bookTitle");
        Mockito.when(bookDataResolver.getBookPrice("book")).thenReturn(BigDecimal.valueOf(1.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book1")).thenReturn("bookTitle1");
        Mockito.when(bookDataResolver.getBookPrice("book1")).thenReturn(BigDecimal.valueOf(0.3).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book2")).thenReturn("bookTitle2");
        Mockito.when(bookDataResolver.getBookPrice("book2")).thenReturn(BigDecimal.valueOf(1.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book3")).thenThrow(OrderBookNotFound.class);
        Mockito.when(bookDataResolver.getBookPrice("book3")).thenThrow(OrderBookNotFound.class);
    }
    
    @Test
    public void test_order_of_id_should_be_ok() {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        assertNotNull(testOrder.getUser());
    }

    @Test
    public void test_zero_or_negative_order_year() {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyyD");
        OrderNumber on1 = OrderNumber.of(0);
        assertEquals(Integer.parseInt(LocalDateTime.now().format(dtf)),on1.toInteger());
        OrderNumber on2 = OrderNumber.of(-1);
        assertEquals(on1.toInteger(),on2.toInteger());
    }

    @Test
    public void test_order_without_userid_should_be_error() {
        assertThrows(NullPointerException.class, () -> Order.of(null));
    }

    @Test
    public void test_order_same_price_not_duplicate_items() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book",1,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        testOrder.addItem(OrderItem.of("book",2,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        assertEquals(2,testOrder.getTotalAmount());
        assertEquals(USER_ID1,testOrder.getUser().getUserId());
        assertEquals(BigDecimal.valueOf(2.4).setScale(2),testOrder.getTotalPrice());
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
    }

    @Test
    public void test_order_remove_items() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book",1,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        testOrder.addItem(OrderItem.of("book",0,bookDataResolver));
        assertEquals(0,testOrder.getItems().size());
        assertEquals(0,testOrder.getTotalAmount());
        assertEquals(USER_ID1,testOrder.getUser().getUserId());
        assertEquals(BigDecimal.valueOf(0),testOrder.getTotalPrice());
    }


    @Test
    public void test_order_different_price_duplicate_items() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book",1,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        Mockito.when(bookDataResolver.getBookPrice("book")).thenReturn(BigDecimal.valueOf(2.2).setScale(2));
        testOrder.addItem(OrderItem.of("book",1,bookDataResolver));
        assertEquals(2,testOrder.getItems().size());
        assertEquals(2,testOrder.getTotalAmount());
        assertEquals(USER_ID1,testOrder.getUser().getUserId());
        assertEquals(BigDecimal.valueOf(3.4).setScale(2),testOrder.getTotalPrice());
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
    }


    @Test
    public void test_order_not_accumulate_amount_of_items() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book",3,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        testOrder.addItem(OrderItem.of("book",2,bookDataResolver));
        assertEquals(1,testOrder.getItems().size());
        assertEquals(2, testOrder.getTotalAmount());
        assertEquals(USER_ID1,testOrder.getUser().getUserId());
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
    }

    @Test
    public void test_immutable_items() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book",3,bookDataResolver));
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
        assertThrows(UnsupportedOperationException.class, () -> testOrder.getItems().add(OrderItem.of("BadBook",3,bookDataResolver)));
    }

    @Test
    public void test_order_price() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book1",3,bookDataResolver));
        testOrder.addItem(OrderItem.of("book2",2,bookDataResolver));
        assertEquals(2,testOrder.getItems().size());
        assertEquals(5, testOrder.getTotalAmount());
        assertEquals(BigDecimal.valueOf(3.30).setScale(2), testOrder.getTotalPrice());
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
    }

    @Test
    public void test_order_item_price() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book1",3,bookDataResolver));
        assertEquals(BigDecimal.valueOf(0.3).setScale(2), testOrder.getItems().stream().map(OrderItem::getPrice).reduce(BigDecimal.ZERO, BigDecimal::add));
        assertTrue(testOrder.getItems().stream().map(OrderItem::isValid).reduce(Boolean.TRUE,(a,b) -> a && b).booleanValue());
    }

    @Test
    public void test_add_missing_book() throws OrderBookNotFound {
        Order testOrder = Order.of(OrderUser.of(USER_ID1,USER_NICK1,USER_EMAIL1));
        testOrder.addItem(OrderItem.of("book1",3,bookDataResolver));
        assertThrows(OrderBookNotFound.class, () -> testOrder.addItem(OrderItem.of("book3",3,bookDataResolver)));
        assertEquals(1,testOrder.getItems().size());
    }

}
