package com.example.ddd.demo.orders;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.ddd.demo.orders.adapters.rest.dto.OrderDTO;
import com.example.ddd.demo.orders.adapters.rest.dto.OrderItemDTO;
import com.example.ddd.demo.orders.domain.exception.OrderBookNotFound;
import com.example.ddd.demo.orders.domain.model.OrderBookDataResolver;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import static com.example.ddd.demo.orders.OrderRestCommandUseCaseTests.MY_TEST_USER;
import static com.example.ddd.demo.orders.OrderRestCommandUseCaseTests.MY_ADMIN_USER;
import static com.example.ddd.demo.orders.OrderRestCommandUseCaseTests.VALID_SCOPE;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
@Transactional
public class OrderRestQueryUseCaseTests {

    public static String BASE_PATH = "/v1/order/";
    public static String BASE_LINK_FORMAT = "http://localhost"+BASE_PATH;
    public static String LINK_FORMAT = BASE_LINK_FORMAT + "?page=%s&size=3";

    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private OrderBookDataResolver bookDataResolver;

    @BeforeEach
    public void setup() throws OrderBookNotFound {
        Mockito.when(bookDataResolver.getBookTitle("book001")).thenReturn("bookTitle001");
        Mockito.when(bookDataResolver.getBookPrice("book001")).thenReturn(BigDecimal.valueOf(1.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book002")).thenReturn("bookTitle002");
        Mockito.when(bookDataResolver.getBookPrice("book002")).thenReturn(BigDecimal.valueOf(2.2).setScale(2));
        Mockito.when(bookDataResolver.getBookTitle("book003")).thenReturn("bookTitle003");
        Mockito.when(bookDataResolver.getBookPrice("book003")).thenReturn(BigDecimal.valueOf(10.2).setScale(2));
        objectMapper.registerModule(new Jackson2HalModule());
    }

    @Test
    public void test_asking_for_undefined_order_should_return_404() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"0")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_asking_for_one_order_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"100")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        OrderDTO orderTest = objectMapper.readValue(content, OrderDTO.class);
        assertEquals(4, orderTest.getTotalAmount());
        assertEquals(BigDecimal.valueOf(15.80).setScale(2), orderTest.getTotalPrice());
        assertEquals(2,orderTest.getItems().stream()
            .filter(i->i.getBookId().equals("book002") 
                    && i.getPrice().equals(BigDecimal.valueOf(2.2).setScale(2)))
            .map(OrderItemDTO::getAmount)
            .reduce(Integer::sum).get());
    }

    private void checkPage(PagedModel<EntityModel<OrderDTO>> page, String nxt, String prv, long idCheck) {
        assertNotNull(page);
        assertEquals(3, page.getMetadata().getSize());
        assertEquals(9, page.getMetadata().getTotalElements());
        assertEquals(3, page.getMetadata().getTotalPages());
        assertEquals(3, page.getContent().size());
        // Check User Email
        assertEquals("email1@a.com", page.getContent().stream().findFirst().get().getContent().getUserEmail());
        // Check Book Title
        assertEquals("bookTitle001", page.getContent().stream().findFirst().get().getContent().getItems().stream().findFirst().get().getTitle());
        // Check Order Price
        assertEquals(BigDecimal.valueOf(15.80).setScale(2), page.getContent().stream().findFirst().get().getContent().getTotalPrice());
        // Check first item price
        assertEquals(BigDecimal.valueOf(1.2).setScale(2), page.getContent().stream().findFirst().get().getContent().getItems().stream().findFirst().get().getPrice());
        // Check HAL links
        if (nxt!=null) assertEquals(String.format(LINK_FORMAT,nxt), page.getLink("next").get().getHref());
        else assertEquals(Optional.empty(),page.getNextLink());
        if (prv!=null) assertEquals(String.format(LINK_FORMAT,prv), page.getLink("prev").get().getHref());
        else assertEquals(Optional.empty(),page.getPreviousLink());
        assertEquals(BASE_LINK_FORMAT, page.getLink("post").get().getHref());
        EntityModel<OrderDTO> anyOrder = page.getContent().stream().findAny().orElse(null);
        assertNotNull(anyOrder);
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("self").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("put").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("delete").get().getHref());
    }

    @Test
    public void test_asking_for_all_orders_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<OrderDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<OrderDTO>>>(){});
        checkPage(page,"1",null,100L);
    }

    @Test
    public void test_asking_for_invalid_page_should_not_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=-1")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_asking_for_an_order_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"100")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<OrderDTO> orderDTO = objectMapper.readValue(content, new TypeReference<EntityModel<OrderDTO>>(){});
        assertNotNull(orderDTO);
    }


    @Test
    public void test_asking_for_page_0_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=0")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<OrderDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<OrderDTO>>>(){});
        checkPage(page,"1",null,100L);
    }

    @Test
    public void test_asking_for_page_1_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=1")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<OrderDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<OrderDTO>>>(){});
        checkPage(page,"2","0",400L);
    }

    @Test
    public void test_asking_for_page_2_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=2")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<OrderDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<OrderDTO>>>(){});
        checkPage(page,null,"1",700L);
    }

    @Test
    public void test_asking_for_all_no_admin_should_not_be_authorized() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"all?page=0")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_asking_for_all_admin_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"all?page=0")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<OrderDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<OrderDTO>>>(){});
        assertNotNull(page);
        assertEquals(3, page.getMetadata().getSize());
        assertEquals(12, page.getMetadata().getTotalElements());
        assertEquals(4, page.getMetadata().getTotalPages());
        assertEquals(3, page.getContent().size());
        EntityModel<OrderDTO> anyOrder = page.getContent().stream().findAny().orElse(null);
        assertNotNull(anyOrder);
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("self").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("put").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("delete").get().getHref());
    }

}
