package com.example.ddd.demo.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.ddd.demo.users.adapters.rest.dto.UserDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
@Transactional
public class UsersRestQueryUseCaseTests {

    public static String BASE_PATH = "/v1/user/";
    public static String BASE_LINK_FORMAT = "http://localhost"+BASE_PATH;
    private static String LINK_FORMAT = BASE_LINK_FORMAT + "?page=%s&size=3";

    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setup() {
        objectMapper.registerModule(new Jackson2HalModule());
    }

    @Test
    public void test_asking_for_undefined_user_should_return_404() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "0")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_asking_for_one_order_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "51775fd2-fca6-11ec-b939-0242ac120001")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        UserDTO userTest = objectMapper.readValue(content, UserDTO.class);
        assertEquals("John Doe1", userTest.getFullname());
    }

    private void checkPage(PagedModel<EntityModel<UserDTO>> page, String nxt, String prv) {
        assertNotNull(page);
        assertEquals(3, page.getMetadata().getSize());
        assertEquals(9, page.getMetadata().getTotalElements());
        assertEquals(3, page.getMetadata().getTotalPages());
        assertEquals(3, page.getContent().size());
        if (nxt!=null) assertEquals(String.format(LINK_FORMAT,nxt), page.getLink("next").get().getHref());
        else assertEquals(Optional.empty(),page.getNextLink());
        if (prv!=null) assertEquals(String.format(LINK_FORMAT,prv), page.getLink("prev").get().getHref());
        else assertEquals(Optional.empty(),page.getPreviousLink());
        assertEquals(BASE_LINK_FORMAT, page.getLink("post").get().getHref());
        EntityModel<UserDTO> anyOrder = page.getContent().stream().findAny().orElse(null);
        assertNotNull(anyOrder);
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("self").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("put").get().getHref());
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("delete").get().getHref());
    }

    @Test
    public void test_asking_for_all_users_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<UserDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<UserDTO>>>(){});
        checkPage(page,"1",null);
    }

    @Test
    public void test_asking_for_invalid_page_should_not_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "?page=-1")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_asking_for_one_user_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "51775fd2-fca6-11ec-b939-0242ac120001")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<UserDTO> UserDTO = objectMapper.readValue(content, new TypeReference<EntityModel<UserDTO>>(){});
        assertNotNull(UserDTO);
    }


    @Test
    public void test_asking_for_page_0_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "?page=0")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<UserDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<UserDTO>>>(){});
        checkPage(page,"1",null);
    }

    @Test
    public void test_asking_for_page_1_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "?page=1")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<UserDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<UserDTO>>>(){});
        checkPage(page,"2","0");
    }

    @Test
    public void test_asking_for_page_2_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH + "?page=2")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        PagedModel<EntityModel<UserDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<UserDTO>>>(){});
        checkPage(page,null,"1");
    }

}
