package com.example.ddd.demo.users;

import static org.junit.jupiter.api.Assertions.assertEquals;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.event.ApplicationEvents;
import org.springframework.test.context.event.RecordApplicationEvents;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.example.ddd.demo.users.adapters.rest.dto.UserDTO;
import com.example.ddd.demo.users.application.command.CreateUserCommand;
import com.example.ddd.demo.users.application.command.UpdateUserCommand;
import com.example.ddd.demo.users.domain.event.UsersEvent;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import static com.example.ddd.demo.users.UsersRestQueryUseCaseTests.BASE_LINK_FORMAT;
import static com.example.ddd.demo.users.UsersRestQueryUseCaseTests.BASE_PATH;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@RecordApplicationEvents
@AutoConfigureMockMvc
@Transactional
public class UsersRestCommandUseCaseTests {

    public static String VALID_SCOPE = "SCOPE_micampus";
    public static String MY_TEST_USER = "51775fd2-fca6-11ec-b939-0242ac120008";
    public static String MY_ADMIN_USER = "51775fd2-fca6-11ec-b939-0242ac120009";

    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @Autowired 
    private ApplicationEvents applicationEvents;


    @BeforeEach
    public void setup() {
        objectMapper.registerModule(new Jackson2HalModule());
    }


    private void checkHateoasLinks(EntityModel<UserDTO> entity) {
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("put").get().getHref());
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("delete").get().getHref());
        assertEquals(BASE_LINK_FORMAT+entity.getContent().getId(), entity.getLink("self").get().getHref());
    }

    /*
     * CREATE METHODS
     */

    @Test
    public void test_create_new_empty_user_should_be_400() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(400, status);
    }

    @Test
    public void test_create_new_invalid_json_should_be_406() throws Exception {
        String requestJson = "{ \"user\": 10, \"value\": 11 }";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_create_new_user_as_regular_user_should_be_403() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateUserCommand.of("changefull","myregularnick","change@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_create_new_user_should_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateUserCommand.of("mifullname","mynick","mi@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<UserDTO> userTest = objectMapper.readValue(content, new TypeReference<EntityModel<UserDTO>>(){});
        assertEquals(36, userTest.getContent().getId().length());
        assertEquals("mynick", userTest.getContent().getNick());
        assertEquals("mifullname", userTest.getContent().getFullname());
        //assertEquals("mi@email.com", userTest.getContent().getEmail());
        checkHateoasLinks(userTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(UsersEvent.class)
                .filter(event -> event.getTarget().getFullName().equals("mifullname")
                                && event.isNew())
                .count());
    }

    @Test
    public void test_create_new_invalid_user_email_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateUserCommand.of("mifullname","nickname","myemail.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_create_new_invalid_user_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(CreateUserCommand.of("mifullname","nick","mm@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(BASE_PATH)
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    /*
     * UPDATE METHODS
     */

    @Test
    public void test_update_new_empty_user_should_be_not_supported() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(405, status);
    }

    @Test
    public void test_update_new_user_as_regular_should_be_forbidden() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateUserCommand.of("changefull","change@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_update_with_invalid_user_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateUserCommand.of("fn","email"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_update_with_invalid_json_should_not_be_ok() throws Exception {
        String requestJson = "{ \"user\": 10, \"value\": 11 }";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(406, status);
    }

    @Test
    public void test_update_no_user_should_not_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateUserCommand.of("changefull","change@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"999")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_update_user_should_be_ok() throws Exception {
        String requestJson = objectMapper.writer().withDefaultPrettyPrinter()
            .writeValueAsString(UpdateUserCommand.of("changefull","change@email.com"));
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .contentType(MediaType.APPLICATION_JSON_VALUE)
            .content(requestJson)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<UserDTO> userTest = objectMapper.readValue(content, new TypeReference<EntityModel<UserDTO>>(){});
        assertEquals("51775fd2-fca6-11ec-b939-0242ac120001", userTest.getContent().getId());
        assertEquals("nickjdo1", userTest.getContent().getNick());
        assertEquals("changefull", userTest.getContent().getFullname());
        checkHateoasLinks(userTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(UsersEvent.class)
                .filter(event -> event.getTarget().getFullName().equals("changefull")
                                && event.isUpdate())
                .count());
    }

    /*
     * DELETE METHODS
     */

    @Test
    public void test_delete_empty_user_should_be_not_supported() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(405, status);
    }

    @Test
    public void test_delete_non_admin_user_should_be_forbidden() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_delete_no_user_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"999")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);
    }

    @Test
    public void test_delete_user_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_ADMIN_USER))
                .authorities(new SimpleGrantedAuthority(VALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        EntityModel<UserDTO> userTest = objectMapper.readValue(content, new TypeReference<EntityModel<UserDTO>>(){});
        assertEquals("51775fd2-fca6-11ec-b939-0242ac120001", userTest.getContent().getId());
        assertEquals("nickjdo1", userTest.getContent().getNick());
        assertEquals("John Doe1", userTest.getContent().getFullname());
        checkHateoasLinks(userTest);
        // Event is launched
        assertEquals(1,applicationEvents
                .stream(UsersEvent.class)
                .filter(event -> event.getTarget().getFullName().equals("John Doe1")
                                && event.isDelete())
                .count());

        // The user is not present anymore
        mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"51775fd2-fca6-11ec-b939-0242ac120001")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        status = mvcResult.getResponse().getStatus();
        assertEquals(404, status);

    }

}
