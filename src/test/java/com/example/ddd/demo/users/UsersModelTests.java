package com.example.ddd.demo.users;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.time.LocalDateTime;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.example.ddd.demo.users.domain.model.User;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class UsersModelTests {
    
    @Test
    public void test_user_with_invalid_data() {
        // Invalid email
        assertThrows(IllegalArgumentException.class,() -> User.of("fullname","nickname","email"));
        // Invalid nick
        assertThrows(IllegalArgumentException.class,() -> User.of("fullname","nick","email@a.com"));
    }

    @Test
    public void test_user_with_valid_data() {
        User u = User.of("fullname","nickname","email@aa.es");
        assertEquals(36,u.getId().length());
        assertEquals(LocalDateTime.now().getDayOfWeek(),u.getStartDate().getDayOfWeek());
    }

}
