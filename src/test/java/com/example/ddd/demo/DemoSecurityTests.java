package com.example.ddd.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class DemoSecurityTests {

    private static String MY_TEST_USER = "notvalid";
    private static String INVALID_SCOPE = "notvalid";

    private static String USER_ID_PATH = "/v1/user/51775fd2-fca6-11ec-b939-0242ac120001";
    private static String ORDER_ID_PATH = "/v1/order/100";

    @Autowired
    private MockMvc mvc;

    /*
     * API-DOCS
     */

    @Test
    public void test_apidocs_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api-docs")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    /*
     * CATALOG
     */
    
    @Test
    public void test_catalog_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/v1/catalog/")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    /*
     * USER
     */

    @Test
    public void test_users_get_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(USER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }

    @Test
    public void test_users_post_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(USER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_users_post_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(USER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_users_put_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(USER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_users_put_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(USER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_users_delete_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(USER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_users_delete_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.delete(USER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    /*
     * ORDER
     */

    @Test
    public void test_order_get_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(ORDER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_order_get_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(ORDER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_order_put_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(ORDER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_order_put_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.put(ORDER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

    @Test
    public void test_order_post_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(ORDER_ID_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(401, status);
    }

    @Test
    public void test_order_post_not_valid_scope_should_not_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.post(ORDER_ID_PATH)
            .with(SecurityMockMvcRequestPostProcessors.jwt()
                .jwt(u->u.subject(MY_TEST_USER))
                .authorities(new SimpleGrantedAuthority(INVALID_SCOPE)))
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(403, status);
    }

}

