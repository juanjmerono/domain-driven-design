package com.example.ddd.demo;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpMethod;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
public class DemoDocTests {

	@Autowired
    private ObjectMapper objectMapper;

    @Autowired
    private MockMvc mvc;

    @Test
    public void test_apidocs_should_be_ok() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api-docs")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
    }
    
    private void checkOpenApiPath(Map<String,Object> paths, String path, String method, boolean priv) {
        assertEquals(path,paths.keySet().stream().filter(k->path.equals(k)).findAny().orElse("notfound"),"OpenAPI Check EndPoint Path");
        assertEquals(method,((Map<String,String>)paths.get(path)).keySet().stream().filter(k->method.equals(k)).findAny().orElse("notvalid"),"OpenAPI Check EndPoint Method");
        List<String> details = ((Map<String,Object>)((Map<String,Object>)paths.get(path)).get(method)).keySet().stream().collect(Collectors.toList());
        assertEquals(priv?"security":"notsecurity",details.stream().filter(k->"security".equals(k)).findAny().orElse("notsecurity"),"OpenAPI Check EndPoint Authentication");
    }

    /*
     * ORDER v1 DOCUMENTATION
     */

    @Test
    void test_order_should_be_documented() throws Exception {
        String apiPath = "/v1/order";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api-docs")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Map<String,Object> openapi = objectMapper.readValue(content, new TypeReference<Map<String,Object>>(){});
        // Check format and test server
        assertEquals("3.0.1",openapi.get("openapi"),"OpenAPI Version 3.0.1");
        assertEquals("http://localhost",((List<Map<String,String>>)openapi.get("servers")).get(0).get("url"),"OpenAPI Test Server URL");
        // Check documented endpoints
        Map<String,Object> paths = (Map<String,Object>)openapi.get("paths");
        // Orders
        checkOpenApiPath(paths, apiPath + "/", HttpMethod.GET.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/all", HttpMethod.GET.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/", HttpMethod.POST.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.GET.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.PUT.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.DELETE.toString().toLowerCase(), true);
    }

    /*
     * CATALOG v1 DOCUMENTATION
     */

    @Test
    void test_catalog_should_be_documented() throws Exception {
        String apiPath = "/v1/catalog";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api-docs")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Map<String,Object> openapi = objectMapper.readValue(content, new TypeReference<Map<String,Object>>(){});
        // Check format and test server
        assertEquals("3.0.1",openapi.get("openapi"),"OpenAPI Version 3.0.1");
        assertEquals("http://localhost",((List<Map<String,String>>)openapi.get("servers")).get(0).get("url"),"OpenAPI Test Server URL");
        // Check documented endpoints
        Map<String,Object> paths = (Map<String,Object>)openapi.get("paths");
        // Orders
        checkOpenApiPath(paths, apiPath + "/", HttpMethod.GET.toString().toLowerCase(), false);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.GET.toString().toLowerCase(), false);
    }

    /*
     * USER v1 DOCUMENTATION
     */

    @Test
    void test_user_should_be_documented() throws Exception {
        String apiPath = "/v1/user";
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get("/api-docs")
           .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();
        
        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        Map<String,Object> openapi = objectMapper.readValue(content, new TypeReference<Map<String,Object>>(){});
        // Check format and test server
        assertEquals("3.0.1",openapi.get("openapi"),"OpenAPI Version 3.0.1");
        assertEquals("http://localhost",((List<Map<String,String>>)openapi.get("servers")).get(0).get("url"),"OpenAPI Test Server URL");
        // Check documented endpoints
        Map<String,Object> paths = (Map<String,Object>)openapi.get("paths");
        // Orders
        checkOpenApiPath(paths, apiPath + "/", HttpMethod.GET.toString().toLowerCase(), false);
        checkOpenApiPath(paths, apiPath + "/", HttpMethod.POST.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.GET.toString().toLowerCase(), false);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.PUT.toString().toLowerCase(), true);
        checkOpenApiPath(paths, apiPath + "/{id}", HttpMethod.DELETE.toString().toLowerCase(), true);
    }

}

