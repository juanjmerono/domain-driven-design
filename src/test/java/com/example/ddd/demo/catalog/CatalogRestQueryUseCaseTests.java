package com.example.ddd.demo.catalog;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import javax.transaction.Transactional;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.hateoas.EntityModel;
import org.springframework.hateoas.PagedModel;
import org.springframework.hateoas.mediatype.hal.Jackson2HalModule;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.web.client.RestTemplate;

import com.example.ddd.demo.catalog.adapters.rapi.entity.AuthorAPI;
import com.example.ddd.demo.catalog.adapters.rapi.entity.BookAPI;
import com.example.ddd.demo.catalog.adapters.rapi.entity.CatalogPageAPI;
import com.example.ddd.demo.catalog.adapters.rest.dto.BookDTO;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
@AutoConfigureMockMvc
@Transactional
public class CatalogRestQueryUseCaseTests {

    private static String BASE_PATH = "/v1/catalog/";
    private static String BASE_LINK_FORMAT = "http://localhost"+BASE_PATH;
    private static String LINK_FORMAT = BASE_LINK_FORMAT+"?page=%s&size=32";

    @Autowired
    private MockMvc mvc;

	@Autowired
    private ObjectMapper objectMapper;

    @Value("${gutendex.api.url}")
    private String apiGutendex;

    @MockBean
    private RestTemplate restTemplateTest;

    @BeforeEach
    public void setup() {
        List<BookAPI> bookList = new ArrayList<>();
        Map<String,String> f = new HashMap<String,String>();
        f.put("image/jpeg","http://cover");
        f.put("text/html","http://download");
        for (int k = 0; k<32; k++) {
            bookList.add(BookAPI.builder()
                .id(k)
                .title("One title")
                .authors(new ArrayList<AuthorAPI>())
                .downloads(1510L)
                .formats(f)
                .build());
        }

        ResponseEntity<CatalogPageAPI> responseEntity1 = (ResponseEntity<CatalogPageAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntity1.getBody()).thenReturn(CatalogPageAPI.builder()
            .count(96)
            .next("/catalog/?page=2")
            .results(bookList)
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiGutendex + "/?page=1", CatalogPageAPI.class)).thenReturn(responseEntity1);

        ResponseEntity<CatalogPageAPI> responseEntity2 = (ResponseEntity<CatalogPageAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntity2.getBody()).thenReturn(CatalogPageAPI.builder()
            .count(96)
            .next("/catalog/?page=3")
            .previous("/catalog/?page=1")
            .results(bookList)
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiGutendex + "/?page=2", CatalogPageAPI.class)).thenReturn(responseEntity2);

        ResponseEntity<CatalogPageAPI> responseEntity3 = (ResponseEntity<CatalogPageAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntity3.getBody()).thenReturn(CatalogPageAPI.builder()
            .count(96)
            .previous("/catalog/?page=2")
            .results(bookList)
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiGutendex + "/?page=3", CatalogPageAPI.class)).thenReturn(responseEntity3);

        ResponseEntity<CatalogPageAPI> responseEntityOne = (ResponseEntity<CatalogPageAPI>)Mockito.mock(ResponseEntity.class);
        Mockito.when(responseEntityOne.getBody()).thenReturn(CatalogPageAPI.builder()
            .count(1)
            .results(bookList)
            .build());
        Mockito.when(restTemplateTest.getForEntity(apiGutendex + "/?ids=100", CatalogPageAPI.class)).thenReturn(responseEntityOne);
        
    }

    private void checkPage(PagedModel<EntityModel<BookDTO>> page, String nxt, String prv) {
        assertNotNull(page);
        assertEquals(32, page.getMetadata().getSize());
        assertEquals(96, page.getMetadata().getTotalElements());
        assertEquals(32, page.getContent().size());
        assertEquals(1510L, page.getContent().stream().filter(i->i.getContent().getId()==1).map(i->i.getContent().getDownloads()).reduce(Long.valueOf(0),Long::sum));
        assertEquals(BigDecimal.valueOf(15.10).setScale(2), page.getContent().stream().filter(i->i.getContent().getId()==1).map(i->i.getContent().getPrice()).reduce(BigDecimal.ZERO,BigDecimal::add));
        assertEquals(3, page.getMetadata().getTotalPages());
        assertEquals("http://cover",page.getContent().stream().filter(i->i.getContent().getId()==1).map(i->i.getContent().getUrlCover()).reduce("",String::concat));
        assertEquals("http://download",page.getContent().stream().filter(i->i.getContent().getId()==1).map(i->i.getContent().getUrlDownload()).reduce("",String::concat));
        assertEquals("One title",page.getContent().stream().filter(i->i.getContent().getId()==1).map(i->i.getContent().getTitle()).reduce("",String::concat));
        if (nxt!=null) assertEquals(String.format(LINK_FORMAT,nxt), page.getLink("next").get().getHref());
        else assertEquals(Optional.empty(),page.getNextLink());
        if (prv!=null) assertEquals(String.format(LINK_FORMAT,prv), page.getLink("prev").get().getHref());
        else assertEquals(Optional.empty(),page.getPreviousLink());
        EntityModel<BookDTO> anyOrder = page.getContent().stream().findAny().orElse(null);
        assertNotNull(anyOrder);
        assertEquals(BASE_LINK_FORMAT+anyOrder.getContent().getId(), anyOrder.getLink("self").get().getHref());
    }

    @Test
    public void test_asking_for_default_page_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH)
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        objectMapper.registerModule(new Jackson2HalModule());
        PagedModel<EntityModel<BookDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<BookDTO>>>(){});
        checkPage(page, "1", null);
    }

    @Test
    public void test_asking_for_first_page_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=0")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        objectMapper.registerModule(new Jackson2HalModule());
        PagedModel<EntityModel<BookDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<BookDTO>>>(){});
        checkPage(page, "1", null);
    }
    
    @Test
    public void test_load_second_page_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=1")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        objectMapper.registerModule(new Jackson2HalModule());
        PagedModel<EntityModel<BookDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<BookDTO>>>(){});
        checkPage(page, "2", "0");
    }

    @Test
    public void test_load_last_page_should_be_valid() throws Exception {
        MvcResult mvcResult = mvc.perform(MockMvcRequestBuilders.get(BASE_PATH+"?page=2")
            .accept(MediaType.APPLICATION_JSON_VALUE)).andReturn();

        int status = mvcResult.getResponse().getStatus();
        assertEquals(200, status);
        String content = mvcResult.getResponse().getContentAsString();
        objectMapper.registerModule(new Jackson2HalModule());
        PagedModel<EntityModel<BookDTO>> page = objectMapper.readValue(content, new TypeReference<PagedModel<EntityModel<BookDTO>>>(){});
        checkPage(page, null, "1");
    }

}
