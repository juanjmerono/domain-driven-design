package com.example.ddd.demo.catalog;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.junit.jupiter.api.Assertions.assertThrows;

import java.math.BigDecimal;
import java.util.Arrays;

import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestPropertySource;

import com.example.ddd.demo.catalog.domain.models.Author;
import com.example.ddd.demo.catalog.domain.models.Book;

@SpringBootTest
@TestPropertySource("classpath:test.properties")
public class CatalogModelTests {

    @Test
    public void test_price_should_be_related_to_downloads() {
        Book b = Book.of(0, 
            "title", 
            Arrays.asList(new Author[]{}), 
            "http://cover",
            "http://downloads", 
            1500);
        assertNotNull(b);
        assertEquals(BigDecimal.valueOf(15).setScale(2), b.getPrice());
    }

    @Test
    public void test_avoid_book_without_title() {
        assertThrows(NullPointerException.class, 
            () -> Book.of(0, 
                    null, 
                        Arrays.asList(new Author[]{}), 
            "http://cover",
        "http://downloads", 
            1500));
    }

    @Test
    public void test_avoid_book_without_authors() {
        assertThrows(NullPointerException.class, 
            () -> Book.of(0, 
                    "title", 
                        null, 
            "http://cover",
        "http://downloads", 
            1500));
    }

    @Test
    public void test_avoid_book_without_urls() {
        assertThrows(NullPointerException.class, 
            () -> Book.of(0, 
                    "title", 
                        Arrays.asList(new Author[]{}), 
            null,
        "http://downloads", 
            1500));
            assertThrows(NullPointerException.class, 
            () -> Book.of(0, 
                    "title", 
                        Arrays.asList(new Author[]{}), 
            "http://cover",
        null, 
            1500));
    }

}
