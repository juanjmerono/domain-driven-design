-- create sequence hibernate_sequence start with 1 increment by 1
-- create table order_items (order_id bigint not null, amount integer, book_id varchar(255), price numeric(19,2))
-- create table orders (id bigint not null, number integer not null, order_user_id varchar(255) not null, primary key (id))
-- create table order_user_entity (id varchar(255) not null, email varchar(255), nick varchar(255), primary key (id))
-- alter table order_items add constraint FKbioxgbv59vetrxe0ejfubep1w foreign key (order_id) references orders
-- alter table orders add constraint FK4m94sonyenvt8uv160y5igpwc foreign key (order_user_id) references order_user_entity

insert into order_user_entity (id,email,nick) values ('51775fd2-fca6-11ec-b939-0242ac120001','email1@a.com','nickjdo1');
insert into order_user_entity (id,email,nick) values ('51775fd2-fca6-11ec-b939-0242ac120002','email2@a.com','nickjdo2');
insert into order_user_entity (id,email,nick) values ('51775fd2-fca6-11ec-b939-0242ac120003','email3@a.com','nickjdo3');

insert into orders (id,number,order_user_id) values (100,2022100,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (200,2022101,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (300,2022102,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (400,2022102,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (500,2022103,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (600,2022103,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (700,2022104,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (800,2022105,'51775fd2-fca6-11ec-b939-0242ac120001');
insert into orders (id,number,order_user_id) values (900,2022105,'51775fd2-fca6-11ec-b939-0242ac120001');

insert into orders (id,number,order_user_id) values (101,2022105,'51775fd2-fca6-11ec-b939-0242ac120002');
insert into orders (id,number,order_user_id) values (201,2022105,'51775fd2-fca6-11ec-b939-0242ac120002');
insert into orders (id,number,order_user_id) values (301,2022102,'51775fd2-fca6-11ec-b939-0242ac120003');

insert into order_items (order_id, amount, price, book_id) values (101, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (101, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (101, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (100, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (100, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (100, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (200, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (200, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (200, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (201, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (201, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (201, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (300, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (300, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (300, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (400, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (400, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (400, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (500, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (500, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (500, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (600, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (600, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (600, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (700, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (700, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (700, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (800, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (800, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (800, 1, 10.2, 'book003');

insert into order_items (order_id, amount, price, book_id) values (900, 1, 1.2, 'book001');
insert into order_items (order_id, amount, price, book_id) values (900, 2, 2.2, 'book002');
insert into order_items (order_id, amount, price, book_id) values (900, 1, 10.2, 'book003');

-- create table users (id varchar(255) not null, email varchar(255), fullname varchar(255), nick varchar(255), start timestamp, primary key (id))

insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120001','email1@a.com','John Doe1','nickjdo1',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120002','email2@a.com','John Doe2','nickjdo2',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120003','email3@a.com','John Doe3','nickjdo3',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120004','email4@a.com','John Doe4','nickjdo4',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120005','email5@a.com','John Doe5','nickjdo5',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120006','email6@a.com','John Doe6','nickjdo6',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120007','email7@a.com','John Doe7','nickjdo7',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120008','email8@a.com','John Doe8','nickjdo8',CURRENT_TIMESTAMP());
insert into users (id, email, fullname, nick, start) values ('51775fd2-fca6-11ec-b939-0242ac120009','email9@a.com','John Doe9','nickjdo9',CURRENT_TIMESTAMP());